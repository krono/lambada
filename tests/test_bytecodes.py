#!/usr/bin/env python
# -*- coding: utf-8 -*-

from lambada.ocaml.bytecodes import (Instruction, get_bytecode,
                                     InvalidBytecode
)
from lambada.interpreter import Interpreter, CamlExit, CamlJump

from lambada.ocaml.primitives import wrap_primitive

from .helper import w_constructor, tag

import util.debug

from struct import pack, unpack

import pytest


def instruct(interpreter, i, wrapped=False, unsigned=False):
    def instruct_(i):
        if unsigned: assert i > 0
        if wrapped:
            interpreter.instructions.append((i << 1) + 1)
        else:
            interpreter.instructions.append(i)
    if isinstance(i, list):
        for el in i:
            instruct_(el)
    else:
        instruct_(i)

def setup_prim(interp, func):
    from lambada.ocaml import primitives
    num_prims = len(primitives.prim_table)
    primitives.prim_table.append(func)
    return num_prims

def bytecode(bc, should_branch=False, ignore_jump=False):
    def _decorate(fn):
        bcs = bc
        if isinstance(bcs, list):
            # all bytecodes in a list must have the same length, so pick one
            base = bcs[0]
            ops = base.get_operand_count()
            def wrapped(self, interpreter, b):
                old_pc = interpreter.pc
                try:
                    fn(self, interpreter, b.index() - base.index())
                except NotImplementedError:
                    pytest.xfail('Test available but bytecode unimplemented')
                if ops != -1 and not should_branch and not ignore_jump:
                    assert interpreter.pc == interpreter._pc_increment(old_pc,
                                                                       ops)
            wrapped.__name__ = fn.__name__
            return pytest.mark.parametrize("b", bcs)(wrapped)
        else:
            ops = bc.get_operand_count()
            def wrapped(self, interpreter, b):
                old_pc = interpreter.pc
                try:
                    fn(self, interpreter)
                except NotImplementedError:
                    pytest.xfail('Test available but bytecode unimplemented')
                if ops != -1 and not should_branch and not ignore_jump:
                    assert interpreter.pc == interpreter._pc_increment(old_pc,
                                                                       ops)
            wrapped.__name__ = fn.__name__
            return pytest.mark.parametrize("b", [bc])(wrapped)
            # return wrapped
    return _decorate


class TestBytecodes(object):


    def assert_branch(self, interpreter):
        " assert the interpreter wants to jump "
        to = 23 #dummy
        # reference point for jump. this is prior to instructing the interp.
        old_pc = len(interpreter.instructions)
        instruct(interpreter, to)
        interpreter.do_step()
        assert interpreter.pc == to + old_pc

    def assert_no_branch(self, interpreter):
        " assert the interpreter does not jump "
        to = 23 # dummy
        instruct(interpreter, to)
        interpreter.do_step()
        # fall through, @bytecode() does the assert

    def _make_env(self, interpreter):
        items = [interpreter.space.wrap_int(i) for i in range(10)]
        return interpreter.space.make_env(items)

    # BEGIN BYTECODES
    #

    def acc_(self, interpreter, index):
        for i in range(index, -1, -1):
            interpreter._push(interpreter.space.wrap_int(i))
        interpreter.do_step()
        assert interpreter.space.unwrap_int(interpreter.accu) == index

    @bytecode([Instruction.ACC0, Instruction.ACC1, Instruction.ACC2,
               Instruction.ACC3, Instruction.ACC4, Instruction.ACC5,
               Instruction.ACC6, Instruction.ACC7])
    def test_acc_(self, interpreter, index):
        self.acc_(interpreter, index)

    @bytecode(Instruction.ACC)
    def test_acc(self, interpreter):
        index = 10
        instruct(interpreter, index)
        self.acc_(interpreter, index)

    @bytecode([Instruction.PUSH, Instruction.PUSHACC0])
    def test_push_push_semantic(self, interpreter, index):
        w_val = interpreter.space.wrap_int(1)
        interpreter.accu = w_val
        interpreter.do_step()
        assert interpreter._top() == w_val

    def pushacc_(self, interpreter, index):
        interpreter.accu = interpreter.space.wrap_int(15)
        for i in range(index, -1, -1):
            interpreter._push(interpreter.space.wrap_int(i))
        interpreter.do_step()
        if index != 0:
            assert interpreter.space.unwrap_int(interpreter.accu) == index - 1
        else:
            assert interpreter.accu == interpreter._top()
        assert interpreter.space.unwrap_int(interpreter._top()) == 15


    @bytecode([Instruction.PUSH, Instruction.PUSHACC0])
    def test_push_pushacc_semantic(self, interpreter, index):
        self.pushacc_(interpreter, 0)

    @bytecode([Instruction.PUSHACC1,
               Instruction.PUSHACC2, Instruction.PUSHACC3,
               Instruction.PUSHACC4, Instruction.PUSHACC5,
               Instruction.PUSHACC6, Instruction.PUSHACC7])
    def test_pushacc_(self, interpreter, index):
        self.pushacc_(interpreter, index + 1)

    @bytecode(Instruction.PUSHACC)
    def test_pushacc(self, interpreter):
        index = 10
        instruct(interpreter, index)
        self.pushacc_(interpreter, index)

    @bytecode(Instruction.POP)
    def test_pop(self, interpreter):
        for i in range(3):
            interpreter._push(interpreter.space.wrap_int(i))
        instruct(interpreter, 2)
        interpreter.do_step()
        assert interpreter.space.unwrap_int(interpreter._top()) == 0

    @bytecode(Instruction.ASSIGN)
    def test_assign(self, interpreter):
        pytest.skip()
        interpreter.do_step()


    def envacc_(self, interpreter, index):
        instruct(interpreter, index, wrapped=False)
        p_list = [interpreter.space.wrap_int(i) for i in range(index)]
        interpreter.env = interpreter.space.make_closure(0, p_list)
        interpreter.do_step()
        assert interpreter.space.unwrap_int(interpreter.accu) == index - 1

    @bytecode([Instruction.ENVACC1, Instruction.ENVACC2, Instruction.ENVACC3,
               Instruction.ENVACC4])
    def test_envacc_(self, interpreter, index):
        self.envacc_(interpreter, index + 1)

    @bytecode(Instruction.ENVACC)
    def test_envacc(self, interpreter):
        n = 7
        instruct(interpreter, n)
        self.envacc_(interpreter, n)

    def pushenvacc_(self, interpreter, n):
        interpreter.accu = interpreter.space.wrap_int(15)
        self.envacc_(interpreter, n)
        assert interpreter.space.unwrap_int(interpreter._top()) == 15

    @bytecode([Instruction.PUSHENVACC1, Instruction.PUSHENVACC2,
               Instruction.PUSHENVACC3, Instruction.PUSHENVACC4])
    def test_pushenvacc_(self, interpreter, index):
        self.pushenvacc_(interpreter, index + 1)

    @bytecode(Instruction.PUSHENVACC)
    def test_pushenvacc(self, interpreter):
        n = 7
        instruct(interpreter, n)
        self.pushenvacc_(interpreter, n)

    @bytecode(Instruction.PUSH_RETADDR)
    def test_push_retaddr(self, interpreter):
        interpreter.env = interpreter.space.wrap_int(15)
        instruct(interpreter, 3)
        interpreter.extra_args = 5
        interpreter.do_step()
        assert interpreter.space.unwrap_int(interpreter._pop()) == 3 + 1
        assert interpreter.space.unwrap_int(interpreter._pop()) == 15
        assert interpreter.space.unwrap_int(interpreter._pop()) == 5

    @bytecode(Instruction.APPLY, should_branch=True)
    def test_apply(self, interpreter):
        _extra_args = 5
        instruct(interpreter, _extra_args)
        _pc = 23
        _env = interpreter.space.make_closure(_pc, [])
        interpreter.accu = _env

        interpreter.do_step()
        assert interpreter.pc == _pc
        assert interpreter.env == _env
        assert interpreter.extra_args == _extra_args - 1

    @bytecode([Instruction.APPLY1, Instruction.APPLY2, Instruction.APPLY3],
              should_branch=True)
    def test_apply_(self, interpreter, index):
        index += 1
        _extra_args = 5
        _pc = 23
        _env = interpreter.space.make_closure(_pc, [])
        _oldenv = interpreter.space.make_closure(0, [])
        for i in range(index):
            interpreter._push(interpreter.space.wrap_int(i))
        interpreter.extra_args = _extra_args
        interpreter.accu = _env
        _oldpc = interpreter.pc + 1

        interpreter.do_step()
        assert interpreter.pc == _pc
        assert interpreter.env == _env
        assert interpreter.extra_args == index - 1
        for i in range(index):
            assert interpreter.space.unwrap_int(interpreter._pop()) == i
        assert interpreter.space.unwrap_int(interpreter._pop()) == _oldpc
        assert interpreter._pop() == _oldenv
        assert interpreter.space.unwrap_int(interpreter._pop()) == _extra_args

    @bytecode(Instruction.APPTERM, should_branch=True)
    def test_appterm(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode([Instruction.APPTERM1, Instruction.APPTERM2,
               Instruction.APPTERM3], should_branch=True)
    def test_appterm_(self, interpreter, index):
        index += 1
        _num_stack = 5
        _stacknum = 17
        _pc = 23
        _env = interpreter.space.make_closure(_pc, [])
        instruct(interpreter, _num_stack)
        for i in range(_num_stack):
            interpreter._push(interpreter.space.wrap_int(i))
        interpreter.accu = _env
        interpreter.do_step()

        assert interpreter.pc == _pc
        assert interpreter.env == _env
        assert interpreter.space.unwrap_int(
            interpreter._pop()) == _num_stack - index

    @bytecode(Instruction.RETURN, should_branch=True)
    def test_return_noextra(self, interpreter):
        interpreter.extra_args = 0
        _stacknum = 17
        interpreter._push(interpreter.space.wrap_int(_stacknum))
        _extra = 5
        _env = interpreter.space.make_closure(0, [])
        _pc = 23
        interpreter._push(interpreter.space.wrap_int(_extra))
        interpreter._push(_env)
        interpreter._push(interpreter.space.wrap_int(_pc))
        num = 3
        for i in range(num):
            interpreter._push(interpreter.space.wrap_int(i))
        instruct(interpreter, num)

        interpreter.do_step()

        assert interpreter.pc == _pc
        assert interpreter.extra_args == _extra
        assert interpreter.env == _env
        assert interpreter.space.unwrap_int(interpreter._top()) == _stacknum

    @bytecode(Instruction.RETURN, should_branch=True)
    def test_return_extra(self, interpreter):
        interpreter.extra_args = 2
        _stacknum = 17
        interpreter._push(interpreter.space.wrap_int(_stacknum))
        num = 3
        for i in range(num):
            interpreter._push(interpreter.space.wrap_int(i))
        instruct(interpreter, num)
        _pc = 23
        _env = interpreter.space.make_closure(_pc, [])
        interpreter.accu = _env
        interpreter.do_step()

        assert interpreter.pc == _pc
        assert interpreter.extra_args == 1
        assert interpreter.env == _env
        assert interpreter.space.unwrap_int(interpreter._top()) == _stacknum


    @bytecode(Instruction.RESTART)
    def test_restart(self, interpreter):
        _e = interpreter.space.make_closure(0, [])
        num = 5
        _env_cont = [_e] + [interpreter.space.wrap_int(i) for i in range(num)]
        _env = interpreter.space.make_closure(0, _env_cont)
        interpreter.env = _env
        interpreter.do_step()

        assert interpreter.extra_args == num
        for i in reversed(range(num - 1)):
            assert interpreter.space.unwrap_int(interpreter._pop()) == i
        assert interpreter.env == _e

    @bytecode(Instruction.GRAB)
    def test_grab_extra(self, interpreter):
        num = 3
        instruct(interpreter, num)
        interpreter.extra_args = 5
        interpreter.do_step()
        assert interpreter.extra_args == 5 - num

    @bytecode(Instruction.GRAB, should_branch=True)
    def test_grab_noextra(self, interpreter):
        num = 5
        args = 3
        _new_extra = 7
        _new_pc = 23
        _new_env = interpreter.space.make_closure(0, [])
        _old_env = interpreter.space.make_closure(42, [])
        instruct(interpreter, num)
        interpreter.extra_args = args
        interpreter.env = _old_env
        interpreter._push(interpreter.space.wrap_int(_new_extra))
        interpreter._push(_new_env)
        interpreter._push(interpreter.space.wrap_int(_new_pc))
        for i in range(args + 1):
            interpreter._push(interpreter.space.wrap_int(i))
        interpreter.do_step()

        assert interpreter.pc == _new_pc
        assert interpreter.extra_args == _new_extra
        assert interpreter.env == _new_env
        _env = interpreter.accu
        assert interpreter.space.is_closure(_env)
        assert interpreter.space.get_closure_size(_env) == args + 3 - 1
        assert interpreter.space.get_closure_variable(_env, 0) == _old_env
        for i in range(args):
            assert interpreter.space.unwrap_int(
                interpreter.space.get_closure_variable(_env, i + 1)) == args - i

    @bytecode(Instruction.CLOSURE)
    def test_closure(self, interpreter):
        interpreter._push(interpreter.space.wrap_int(15))
        nvars = 2
        instruct(interpreter, nvars)
        dest = 5
        instruct(interpreter, dest)
        interpreter.accu = interpreter.space.wrap_int(3)
        interpreter._push(interpreter.space.wrap_int(4))
        interpreter.do_step()
        res = interpreter.accu
        assert interpreter.space.get_closure_code(res) == 5 + 2
        assert interpreter.space.unwrap_int(
            interpreter.space.get_closure_variable(res, 0)) == 3
        assert interpreter.space.unwrap_int(
            interpreter.space.get_closure_variable(res, 1)) == 4
        assert interpreter.space.unwrap_int(interpreter._pop()) == 15

    @bytecode(Instruction.CLOSUREREC)
    def test_closurerec_1func(self, interpreter):
        _stacktop = 15
        nfuncs = 1
        nvars = 2
        _func0 = 5
        _var1 = 3
        _var2 = 4
        interpreter._push(interpreter.space.wrap_int(_stacktop))
        instruct(interpreter, [nfuncs, nvars, _func0])
        interpreter.accu = interpreter.space.wrap_int(_var1)
        interpreter._push(interpreter.space.wrap_int(_var2))
        interpreter.do_step()
        res = interpreter.accu

        assert interpreter._pop() == res
        assert interpreter.space.get_closure_code(res) == _func0 + 3
        assert interpreter.space.unwrap_int(
            interpreter.space.get_closure_variable(res, 0)) == _var1
        assert interpreter.space.unwrap_int(
            interpreter.space.get_closure_variable(res, 1)) == _var2

        assert interpreter.space.unwrap_int(interpreter._pop()) == _stacktop

    @bytecode(Instruction.CLOSUREREC)
    def test_closurerec_nfunc(self, interpreter):
        _stacktop = 15
        nfuncs = 3
        nvars = 2
        _func0 = 23
        _func1 = 42
        _var1 = 3
        _var2 = 4
        interpreter._push(interpreter.space.wrap_int(_stacktop))
        instruct(interpreter, [nfuncs, nvars, _func0, _func1])
        interpreter.accu = interpreter.space.wrap_int(_var1)
        interpreter._push(interpreter.space.wrap_int(_var2))
        interpreter.do_step()
        res = interpreter.accu
        assert interpreter.space.get_closure_code(res) == _func0 + 3
        # assert interpreter.space.unwrap_int(
        #     interpreter.space.get_closure_variable(res, 0)) ==`
        # assert interpreter.space.unwrap_int(
        #     interpreter.space.get_closure_variable(res, 1)) ==
        # assert interpreter.space.unwrap_int(
        #     interpreter.space.get_closure_variable(res, 2)) ==
        # assert interpreter.space.unwrap_int(
        #     interpreter.space.get_closure_variable(res, 3)) ==

        assert interpreter.space.unwrap_int(
            interpreter.get_closure_variable(res, 4)) == _var1
        assert interpreter.space.unwrap_int(
            interpreter.get_closure_variable(res, 5)) == _var2

        assert interpreter.space.unwrap_int(interpreter._pop()) == _stacktop
        assert False, "TBD"

    @bytecode(Instruction.OFFSETCLOSUREM2)
    def test_offsetclosurem2(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    def offsetclosure0_(self, interpreter):
        _env = interpreter.space.make_closure(0, [])
        interpreter.env = _env
        interpreter.do_step()
        assert interpreter.accu == _env

    @bytecode(Instruction.OFFSETCLOSURE0)
    def test_offsetclosure0(self, interpreter):
        self.offsetclosure0_(interpreter)

    @bytecode(Instruction.OFFSETCLOSURE2)
    def test_offsetclosure2(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.OFFSETCLOSURE)
    def test_offsetclosure(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.PUSHOFFSETCLOSUREM2)
    def test_pushoffsetclosurem2(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.PUSHOFFSETCLOSURE0)
    def test_pushoffsetclosure0(self, interpreter):
        stack_num = 5
        interpreter.accu = interpreter.space.wrap_int(stack_num)
        self.offsetclosure0_(interpreter)
        assert interpreter.space.unwrap_int(interpreter._top()) == 5

    @bytecode(Instruction.PUSHOFFSETCLOSURE2)
    def test_pushoffsetclosure2(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.PUSHOFFSETCLOSURE)
    def test_pushoffsetclosure(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.GETGLOBAL)
    def test_getglobal(self, interpreter):
        idx = 0
        val = interpreter.space.wrap_int(1)
        instruct(interpreter, idx)
        interpreter.space.set_global(idx, val)
        interpreter.do_step()
        assert interpreter.accu == val

    @bytecode(Instruction.PUSHGETGLOBAL)
    def test_pushgetglobal(self, interpreter):
        stack_num = 5
        interpreter.accu = interpreter.space.wrap_int(stack_num)
        idx = 0
        val = interpreter.space.wrap_int(1)
        instruct(interpreter, idx)
        interpreter.space.set_global(idx, val)
        interpreter.do_step()
        assert interpreter.accu == val
        assert interpreter.space.unwrap_int(interpreter._top()) == 5

    @bytecode(Instruction.GETGLOBALFIELD)
    def test_getglobalfield(self, interpreter):
        idx = 0
        num = 1
        val = interpreter.space.make_block([interpreter.space.wrap_int(num)])
        instruct(interpreter, [idx, 0])
        interpreter.space.set_global(idx, val)
        interpreter.do_step()
        assert interpreter.space.unwrap_int(interpreter.accu) == num

    @bytecode(Instruction.PUSHGETGLOBALFIELD)
    def test_pushgetglobalfield(self, interpreter):
        stack_num = 5
        interpreter.accu = interpreter.space.wrap_int(stack_num)
        idx = 0
        num = 1
        val = interpreter.space.make_block([interpreter.space.wrap_int(num)])
        instruct(interpreter, [idx, 0])
        interpreter.space.set_global(idx, val)
        interpreter.do_step()
        assert interpreter.space.unwrap_int(interpreter.accu) == num
        assert interpreter.space.unwrap_int(interpreter._top()) == stack_num


    @bytecode(Instruction.SETGLOBAL)
    def test_setglobal(self, interpreter):
        idx = 0
        val = interpreter.space.wrap_int(1)
        instruct(interpreter, idx)
        interpreter.accu = val
        interpreter.do_step()
        assert interpreter.space.get_global(idx) == val

    def atom_(self, interpreter, num):
        interpreter.do_step()
        assert interpreter.space.atom_number(interpreter.accu) == num

    @bytecode(Instruction.ATOM0)
    def test_atom0(self, interpreter):
        self.atom_(interpreter, 0)

    def atom__(self, interpreter):
        instruct(interpreter, 15)
        self.atom_(interpreter, 15)

    @bytecode(Instruction.ATOM)
    def test_atom(self, interpreter):
        self.atom__(interpreter)

    @bytecode(Instruction.PUSHATOM0)
    def test_pushatom0(self, interpreter):
        interpreter.accu = interpreter.space.wrap_int(17)
        self.atom_(interpreter, 0)
        assert interpreter.space.unwrap_int(interpreter._pop()) == 17

    @bytecode(Instruction.PUSHATOM)
    def test_pushatom(self, interpreter):
        interpreter.accu = interpreter.space.wrap_int(17)
        self.atom__(interpreter)
        assert interpreter.space.unwrap_int(interpreter._pop()) == 17

    def makeblock_(self, interpreter, index):
        tag = 17
        instruct(interpreter, tag)
        interpreter.accu = interpreter.space.wrap_int(15)
        for i in range(index - 1, 0, -1):
            interpreter._push(interpreter.space.wrap_int(i))
        interpreter.do_step()
        w_block = interpreter.accu
        assert interpreter.space.get_block_tag(w_block) == tag
        assert interpreter.space.get_block_size(w_block) == index
        assert interpreter.space.unwrap_int(
            interpreter.space.get_block_item(w_block, 0)) == 15
        if index > 1:
            assert interpreter.space.unwrap_int(
                interpreter.space.get_block_item(w_block, index - 1)) == \
                index - 1

    @bytecode(Instruction.MAKEBLOCK)
    def test_makeblock(self, interpreter):
        index = 5
        instruct(interpreter, index)
        self.makeblock_(interpreter, index)

    @bytecode([Instruction.MAKEBLOCK1, Instruction.MAKEBLOCK2,
               Instruction.MAKEBLOCK3])
    def test_makeblock_(self, interpreter, index):
        # ok, we start at 1
        self.makeblock_(interpreter, index + 1)

    @bytecode(Instruction.MAKEFLOATBLOCK)
    def test_makefloatblock(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    def getfield_(self, interpreter, index, use_float=False):
        if use_float:
            wrap = lambda x: interpreter.space.wrap_float(float(x))
        else:
            wrap = interpreter.space.wrap_int
        l = [wrap(i) for i in range(index + 1)]
        w_block = interpreter.space.make_block(l)
        interpreter.accu = w_block
        interpreter.do_step()
        if use_float:
            assert interpreter.space.unwrap_float(
                interpreter.accu) == float(index)
        else:
            assert interpreter.space.unwrap_int(interpreter.accu) == index

    @bytecode([Instruction.GETFIELD0, Instruction.GETFIELD1,
               Instruction.GETFIELD2, Instruction.GETFIELD3])
    def test_getfield_(self, interpreter, index):
        self.getfield_(interpreter, index)

    @bytecode(Instruction.GETFIELD)
    def test_getfield(self, interpreter):
        index = 7
        instruct(interpreter, index)
        self.getfield_(interpreter, index)

    @bytecode(Instruction.GETFLOATFIELD)
    def test_getfloatfield(self, interpreter):
        index = 7
        instruct(interpreter, index)
        self.getfield_(interpreter, index, use_float=True)

    @bytecode([Instruction.SETFIELD0, Instruction.SETFIELD1,
               Instruction.SETFIELD2, Instruction.SETFIELD3])
    def test_setfield_(self, interpreter, index):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.SETFIELD)
    def test_setfield(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.SETFLOATFIELD)
    def test_setfloatfield(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.VECTLENGTH)
    def test_vectlength(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.GETVECTITEM)
    def test_getvectitem(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.SETVECTITEM)
    def test_setvectitem(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.GETSTRINGCHAR)
    def test_getstringchar(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.SETSTRINGCHAR)
    def test_setstringchar(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.BRANCH, should_branch=True)
    def test_branch(self, interpreter):
        self.assert_branch(interpreter)

    @bytecode(Instruction.BRANCHIF, should_branch=False)
    def test_branchif_no(self, interpreter):
        interpreter.accu = interpreter.space.false()
        self.assert_no_branch(interpreter)

    @bytecode(Instruction.BRANCHIF, should_branch=True)
    def test_branchif_yes(self, interpreter):
        interpreter.accu = interpreter.space.true()
        self.assert_branch(interpreter)

    @bytecode(Instruction.BRANCHIF, should_branch=True)
    def test_branchif_yes_2(self, interpreter):
        interpreter.accu = interpreter.space.wrap_int(100) #not false
        self.assert_branch(interpreter)

    @bytecode(Instruction.BRANCHIF, should_branch=True)
    def test_branchif_yes_3(self, interpreter):
        w_val = interpreter.space.wrap_int(1)
        interpreter.accu = interpreter.space.make_block([w_val]) #not false
        self.assert_branch(interpreter)

    @bytecode(Instruction.BRANCHIFNOT, should_branch=True)
    def test_branchifnot_yes(self, interpreter):
        interpreter.accu = interpreter.space.false()
        self.assert_branch(interpreter)

    @bytecode(Instruction.BRANCHIFNOT, should_branch=False)
    def test_branchifnot_no(self, interpreter):
        interpreter.accu = interpreter.space.true()
        self.assert_no_branch(interpreter)

    @bytecode(Instruction.BRANCHIFNOT, should_branch=False)
    def test_branchifno_no_2(self, interpreter):
        interpreter.accu = interpreter.space.wrap_int(100) #not false
        self.assert_no_branch(interpreter)

    @bytecode(Instruction.BRANCHIFNOT, should_branch=False)
    def test_branchifno_no_3(self, interpreter):
        w_val = interpreter.space.wrap_int(1)
        interpreter.accu = interpreter.space.make_block([w_val]) #not false
        self.assert_no_branch(interpreter)

    @bytecode(Instruction.SWITCH, should_branch=True)
    def test_switch(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.BOOLNOT)
    def test_boolnot(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.PUSHTRAP)
    def test_pushtrap(self, interpreter):
        from lambada.ocaml.stack import stack
        _trapspval = stack(32)
        _extra = 37
        _env = interpreter.space.make_closure(42, [])
        _dest = 3
        instruct(interpreter, _dest)
        interpreter.trap_sp = _trapspval
        _tsp = interpreter.trap_sp
        interpreter.extra_args = _extra
        interpreter.env = _env
        interpreter.do_step()

        assert interpreter.sp == interpreter.trap_sp
        assert interpreter.space.unwrap_int(interpreter._pop()) == _dest + 1
        assert interpreter.space.unwrap_stack(interpreter._pop()) == _tsp
        assert interpreter._pop() == _env
        assert interpreter.space.unwrap_int(interpreter._pop()) == _extra

    @bytecode(Instruction.POPTRAP)
    def test_poptrap(self, interpreter):
        from lambada.ocaml.stack import stack
        _spval = 32
        _sp = stack(32)
        interpreter._push(interpreter.space.wrap_int(_spval))
        interpreter._push(interpreter.space.wrap_int(0))
        interpreter._push(interpreter.space.wrap_int(0))
        interpreter._push(interpreter.space.wrap_stack(_sp))
        interpreter._push(interpreter.space.wrap_int(32))

        interpreter.do_step()
        assert interpreter.space.unwrap_int(interpreter._top()) == _spval
        assert interpreter.trap_sp == _sp

    @bytecode(Instruction.RAISE)
    def test_raise(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.CHECK_SIGNALS)
    def test_check_signals(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode([Instruction.C_CALL1, Instruction.C_CALL2, Instruction.C_CALL3,
               Instruction.C_CALL4, Instruction.C_CALL5])
    def test_c_call_(self, interpreter, index):
        num = index + 1
        val = interpreter.space.wrap_int(1)
        other = interpreter.space.wrap_int(42)
        for i in range(num, 1, -1):
            interpreter._push(interpreter.space.wrap_int(i))
        interpreter.accu = val

        if False: pass
        elif num == 1:
            def f(interpreter, arg1):
                assert arg1 == 1
                assert interpreter.accu == val
                return other
        elif num == 2:
            def f(interpreter, arg1, arg2):
                assert arg1 == 1 and arg2 == 2
                assert interpreter.accu == val
                return other
        elif num == 3:
            def f(interpreter, arg1, arg2, arg3):
                assert arg1 == 1 and arg2 == 2 and arg3 == 3
                assert interpreter.accu == val
                return other
        elif num == 4:
            def f(interpreter, arg1, arg2, arg3, arg4):
                assert arg1 == 1 and arg2 == 2 and arg3 == 3 and arg4 == 4
                assert interpreter.accu == val
                return other
        elif num == 5:
            def f(interpreter, arg1, arg2, arg3, arg4, arg5):
                assert arg1 == 1 and arg2 == 2 and arg3 == 3 and arg4 == 4 \
                    and arg5 == 5
                assert interpreter.accu == val
                return other
        else:
            assert False

        spec = [int] * num
        f = wrap_primitive(unwrap_spec=spec)(f)

        prim_num = setup_prim(interpreter, f)
        instruct(interpreter, prim_num, unsigned=True)
        interpreter.do_step()
        assert interpreter.accu == other

    @bytecode(Instruction.C_CALLN)
    def test_c_calln(self, interpreter):
        num = 7
        val = interpreter.space.wrap_int(1)
        other = interpreter.space.wrap_int(42)
        for i in range(num, 1, -1):
            interpreter._push(interpreter.space.wrap_int(i))
        interpreter.accu = val

        @wrap_primitive()
        def f(interpreter, num_args):
            assert len(num_args) == num
            assert interpreter.accu == val
            return other
        prim_num = setup_prim(interpreter, f)
        instruct(interpreter, num, unsigned=True)
        instruct(interpreter, prim_num, unsigned=True)
        interpreter.do_step()
        assert interpreter.accu == other

    def const_(self, interpreter, index):
        interpreter.do_step()
        assert interpreter.space.unwrap_int(interpreter.accu) == index

    @bytecode([Instruction.CONST0, Instruction.CONST1, Instruction.CONST2,
               Instruction.CONST3])
    def test_const_(self, interpreter, index):
        self.const_(interpreter, index)

    def constint_(self, interpreter):
        instruct(interpreter, 15, wrapped=True)
        self.const_(interpreter, 15)

    @bytecode(Instruction.CONSTINT)
    def test_constint(self, interpreter):
        self.constint_(interpreter)

    @bytecode([Instruction.PUSHCONST0,Instruction.PUSHCONST1,
               Instruction.PUSHCONST2, Instruction.PUSHCONST3])
    def test_pushconst_(self, interpreter, index):
        interpreter.accu = interpreter.space.wrap_int(17)
        self.const_(interpreter, index)
        assert interpreter.space.unwrap_int(interpreter._pop()) == 17

    @bytecode(Instruction.PUSHCONSTINT)
    def test_pushconstint(self, interpreter):
        interpreter.accu = interpreter.space.wrap_int(17)
        self.constint_(interpreter)
        assert interpreter.space.unwrap_int(interpreter._pop()) == 17

    @bytecode(Instruction.NEGINT)
    def test_negint_1(self, interpreter):
        interpreter.accu = interpreter.space.wrap_int(5)
        interpreter.do_step()
        assert interpreter.space.unwrap_int(interpreter.accu) == -5

    @bytecode(Instruction.NEGINT)
    def test_negint_2(self, interpreter):
        interpreter.accu = interpreter.space.wrap_int(-5)
        interpreter.do_step()
        assert interpreter.space.unwrap_int(interpreter.accu) == 5

    def inttest(self, interpreter, op1, op2, res):
        interpreter.accu = interpreter.space.wrap_int(op1)
        interpreter._push(interpreter.space.wrap_int(op2))
        interpreter.do_step()
        assert interpreter.space.unwrap_int(interpreter.accu) == res


    @bytecode(Instruction.ADDINT)
    def test_addint(self, interpreter):
        self.inttest(interpreter, 5, 3, 8)

    @bytecode(Instruction.SUBINT)
    def test_subint(self, interpreter):
        self.inttest(interpreter, 5, 3, 2)

    @bytecode(Instruction.MULINT)
    def test_mulint(self, interpreter):
        self.inttest(interpreter, 5, 3, 15)

    @bytecode(Instruction.DIVINT)
    def test_divint(self, interpreter):
        self.inttest(interpreter, 5, 3, 1)

    @bytecode(Instruction.DIVINT, ignore_jump=True)
    def test_divint_(self, interpreter):
        with pytest.raises(Exception):
            self.inttest(interpreter, 5, 0, 132) # doesnt care

    @bytecode(Instruction.MODINT)
    def test_modint(self, interpreter):
        self.inttest(interpreter, 5, 3, 2)

    @bytecode(Instruction.MODINT, ignore_jump=True)
    def test_modint_(self, interpreter):
        with pytest.raises(Exception):
            self.inttest(interpreter, 5, 0, 132) # doesnt care

    @bytecode(Instruction.ANDINT)
    def test_andint(self, interpreter):
        self.inttest(interpreter, 5, 3, 1)

    @bytecode(Instruction.ORINT)
    def test_orint(self, interpreter):
        self.inttest(interpreter, 5, 3, 7)

    @bytecode(Instruction.XORINT)
    def test_xorint(self, interpreter):
        self.inttest(interpreter, 5, 3, 6)

    @bytecode(Instruction.LSLINT)
    def test_lslint(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.LSRINT)
    def test_lsrint(self, interpreter):
        self.inttest(interpreter, 4, 1, 2)

    @bytecode(Instruction.ASRINT)
    def test_asrint(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.EQ)
    def test_eq(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.NEQ)
    def test_neq(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.LTINT)
    def test_ltint(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.LEINT)
    def test_leint(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.GTINT)
    def test_gtint(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.GEINT)
    def test_geint(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.OFFSETINT)
    def test_offsetint(self, interpreter):
        instruct(interpreter, 2)
        interpreter.accu = interpreter.space.wrap_int(3)
        interpreter.do_step()
        assert interpreter.space.unwrap_int(interpreter.accu) == 3 + 2

    @bytecode(Instruction.OFFSETREF)
    def test_offsetref(self, interpreter):
        w_list = interpreter.space.wrap_list([interpreter.space.wrap_int(5)])
        w_add = interpreter.space.wrap_int(3)
        instruct(interpreter, w_add)
        interpreter.accu = w_list
        interpreter.do_step()
        assert interpreter.is_unit(interpreter.accu)
        assert interpreter.space.unwrap_int(w_list.get_child(0)) == 5 + 3

    @bytecode(Instruction.ISINT)
    def test_isint(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.GETMETHOD)
    def test_getmethod(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    def assert_branch_cmp(self, interpreter, op1, op2):
        instruct(interpreter, (op1 << 1) | 1)
        interpreter.accu = interpreter.space.wrap_int(op2)
        self.assert_branch(interpreter)

    def assert_no_branch_cmp(self, interpreter, op1, op2):
        instruct(interpreter, (op1 << 1) | 1)
        interpreter.accu = interpreter.space.wrap_int(op2)
        self.assert_no_branch(interpreter)


    @bytecode(Instruction.BEQ, should_branch=True)
    def test_beq_yes(self, interpreter):
        self.assert_branch_cmp(interpreter, 1, 1)

    @bytecode(Instruction.BEQ)
    def test_beq_no(self, interpreter):
        self.assert_no_branch_cmp(interpreter, 1, 2)

    @bytecode(Instruction.BNEQ)
    def test_bneq_no(self, interpreter):
        self.assert_no_branch_cmp(interpreter, 1, 1)

    @bytecode(Instruction.BNEQ, should_branch=True)
    def test_bneq_yes(self, interpreter):
        self.assert_branch_cmp(interpreter, 1, 2)

    @bytecode(Instruction.BLTINT, should_branch=True)
    def test_bltint_yes(self, interpreter):
        self.assert_branch_cmp(interpreter, 1, 2)

    @bytecode(Instruction.BLTINT)
    def test_bltint_no(self, interpreter):
        self.assert_no_branch_cmp(interpreter, 2, 1)

    @bytecode(Instruction.BLEINT, should_branch=True)
    def test_bleint_yes(self, interpreter):
        self.assert_branch_cmp(interpreter, 1, 1)

    @bytecode(Instruction.BLEINT, should_branch=True)
    def test_bleint_yes2(self, interpreter):
        self.assert_branch_cmp(interpreter, 1, 2)

    @bytecode(Instruction.BLEINT)
    def test_bleint_no(self, interpreter):
        self.assert_no_branch_cmp(interpreter, 2, 1)

    @bytecode(Instruction.BGTINT, should_branch=True)
    def test_bgtint_yes(self, interpreter):
        self.assert_branch_cmp(interpreter, 2, 1)

    @bytecode(Instruction.BGTINT)
    def test_bgtint_no(self, interpreter):
        self.assert_no_branch_cmp(interpreter, 1, 2)

    @bytecode(Instruction.BGEINT, should_branch=True)
    def test_bgeint_yes(self, interpreter):
        self.assert_branch_cmp(interpreter, 1, 1)

    @bytecode(Instruction.BGEINT, should_branch=True)
    def test_bgeint_yes2(self, interpreter):
        self.assert_branch_cmp(interpreter, 2, 1)

    @bytecode(Instruction.BGEINT)
    def test_bgeint_no(self, interpreter):
        self.assert_no_branch_cmp(interpreter, 1, 2)


    @bytecode(Instruction.ULTINT)
    def test_ultint(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.UGEINT)
    def test_ugeint(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.BULTINT)
    def test_bultint(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.BUGEINT)
    def test_bugeint(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.GETPUBMET)
    def test_getpubmet(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.GETDYNMET)
    def test_getdynmet(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.STOP, ignore_jump=True)
    def test_stop(self, interpreter):
        val = 17
        interpreter.accu = interpreter.space.wrap_int(17)
        try:
            interpreter.do_step()
        except CamlExit as e:
            assert interpreter.space.unwrap_int(e.code) == val

    @bytecode(Instruction.EVENT)
    def test_event(self, interpreter):
        pytest.skip()
        interpreter.do_step()

    @bytecode(Instruction.BREAK)
    def test_break(self, interpreter):
        pytest.skip()
        interpreter.do_step()


    # @bytecode(Instruction.CONSTBYTE, length=2)
    # def test_constbyte(self, interpreter):
    #     instruct_b(interpreter, 43, wrapped=True)
    #     interpreter.do_step()
    #     assert interpreter.accu._value == 43

    # @bytecode(Instruction.CONSTSHORT, length=3)
    # def test_constshort(self, interpreter):
    #     instruct_s(interpreter, 1234, wrapped=True)
    #     interpreter.do_step()
    #     assert interpreter.accu._value == 1234

    # @bytecode(Instruction.GETGLOBAL, length=3)
    # def test_getglobal(self, interpreter):
    #     idx = 0
    #     val = integer(1)
    #     instruct_s(interpreter, idx)
    #     interpreter.space.set_global(idx, val)
    #     interpreter.do_step()
    #     assert interpreter.accu == val

    # @bytecode(Instruction.SETGLOBAL, length=3)
    # def test_setglobal(self, interpreter):
    #     idx = 0
    #     val = integer(1)
    #     instruct_s(interpreter, idx)
    #     interpreter.accu = val
    #     interpreter.do_step()
    #     assert interpreter.space.get_global(idx) == val

    # @bytecode(Instruction.CUR, length=3)
    # def test_cur(self, interpreter):
    #     val = 1
    #     instruct_s(interpreter, val)
    #     interpreter.env = interpreter.space.make_env([integer(3)])
    #     interpreter.do_step()
    #     res = interpreter.accu
    #     assert return_pc(res)._value == 2
    #     assert return_env(res).get_child(0)._value == 3

    # @bytecode(Instruction.SWITCH) # XXX: special?
    # def test_switch(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.BRANCH, length=-1)
    # def test_branch(self, interpreter):
    #     self.assert_branch(interpreter)

    # @bytecode(Instruction.BRANCHIF, length=3)
    # def test_branchif__no(self, interpreter):
    #     interpreter.accu = interpreter.space.atom(0) #false
    #     self.assert_no_branch(interpreter)

    # @bytecode(Instruction.BRANCHIF, length=-1)
    # def test_branchif__yes(self, interpreter):
    #     interpreter.accu = interpreter.space.atom(1) #true
    #     self.assert_branch(interpreter)

    # @bytecode(Instruction.BRANCHIF, length=-1)
    # def test_branchif__yes_2(self, interpreter):
    #     interpreter.accu = integer(1) #not false
    #     self.assert_branch(interpreter)

    # @bytecode(Instruction.BRANCHIFNOT, length=-1)
    # def test_branchifnot__yes(self, interpreter):
    #     interpreter.accu = interpreter.space.atom(0) #false
    #     self.assert_branch(interpreter)

    # @bytecode(Instruction.BRANCHIFNOT, length=3)
    # def test_branchifnot__no(self, interpreter):
    #     interpreter.accu = interpreter.space.atom(1) #true
    #     self.assert_no_branch(interpreter)

    # @bytecode(Instruction.BRANCHIFNOT, length=3)
    # def test_branchifnot__no_2(self, interpreter):
    #     interpreter.accu = integer(1) #not false
    #     self.assert_no_branch(interpreter)

    # @bytecode(Instruction.POPBRANCHIFNOT, length=3)
    # def test_popbranchifnot(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.BRANCHIFNEQTAG, length=4)
    # def test_branchifneqtag(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.BRANCHIFEQ, length=-1)
    # def test_branchifeq__yes(self, interpreter):
    #     interpreter._push(interpreter.space.wrap_int(1))
    #     interpreter.accu = interpreter.space.wrap_int(1)
    #     self.assert_branch(interpreter)

    # @bytecode(Instruction.BRANCHIFEQ, length=3)
    # def test_branchifeq__no(self, interpreter):
    #     interpreter._push(interpreter.space.wrap_int(1))
    #     interpreter.accu = interpreter.space.wrap_int(2)
    #     self.assert_no_branch(interpreter)


    # @bytecode(Instruction.BRANCHIFNEQ, length=3)
    # def test_branchifneq__no(self, interpreter):
    #     interpreter._push(interpreter.space.wrap_int(1))
    #     interpreter.accu = interpreter.space.wrap_int(1)
    #     self.assert_no_branch(interpreter)

    # @bytecode(Instruction.BRANCHIFNEQ, length=-1)
    # def test_branchifneq__yes(self, interpreter):
    #     interpreter._push(interpreter.space.wrap_int(1))
    #     interpreter.accu = interpreter.space.wrap_int(2)
    #     self.assert_branch(interpreter)

    # @bytecode(Instruction.BRANCHIFLT, length=-1)
    # def test_branchiflt__yes(self, interpreter):
    #     interpreter._push(interpreter.space.wrap_int(2))
    #     interpreter.accu = interpreter.space.wrap_int(1)
    #     self.assert_branch(interpreter)

    # @bytecode(Instruction.BRANCHIFLT, length=3)
    # def test_branchiflt__no(self, interpreter):
    #     interpreter._push(interpreter.space.wrap_int(1))
    #     interpreter.accu = interpreter.space.wrap_int(1)
    #     self.assert_no_branch(interpreter)

    # @bytecode(Instruction.BRANCHIFGT, length=-1)
    # def test_branchifgt__yes(self, interpreter):
    #     interpreter._push(interpreter.space.wrap_int(1))
    #     interpreter.accu = interpreter.space.wrap_int(2)
    #     self.assert_branch(interpreter)

    # @bytecode(Instruction.BRANCHIFGT, length=3)
    # def test_branchifgt__no(self, interpreter):
    #     interpreter._push(interpreter.space.wrap_int(1))
    #     interpreter.accu = interpreter.space.wrap_int(1)
    #     self.assert_no_branch(interpreter)

    # @bytecode(Instruction.BRANCHIFLE, length=-1)
    # def test_branchifle__yes_1(self, interpreter):
    #     interpreter._push(interpreter.space.wrap_int(2))
    #     interpreter.accu = interpreter.space.wrap_int(1)
    #     self.assert_branch(interpreter)

    # @bytecode(Instruction.BRANCHIFLE, length=-1)
    # def test_branchifle__yes_2(self, interpreter):
    #     interpreter._push(interpreter.space.wrap_int(1))
    #     interpreter.accu = interpreter.space.wrap_int(1)
    #     self.assert_branch(interpreter)

    # @bytecode(Instruction.BRANCHIFLE, length=3)
    # def test_branchifle__no(self, interpreter):
    #     interpreter._push(interpreter.space.wrap_int(1))
    #     interpreter.accu = interpreter.space.wrap_int(2)
    #     self.assert_no_branch(interpreter)

    # @bytecode(Instruction.BRANCHIFGE, length=-1)
    # def test_branchifge__yes_1(self, interpreter):
    #     interpreter._push(interpreter.space.wrap_int(1))
    #     interpreter.accu = interpreter.space.wrap_int(2)
    #     self.assert_branch(interpreter)

    # @bytecode(Instruction.BRANCHIFGE, length=-1)
    # def test_branchifge__yes_2(self, interpreter):
    #     interpreter._push(interpreter.space.wrap_int(1))
    #     interpreter.accu = interpreter.space.wrap_int(1)
    #     self.assert_branch(interpreter)

    # @bytecode(Instruction.BRANCHIFGE, length=3)
    # def test_branchifge__no(self, interpreter):
    #     interpreter._push(interpreter.space.wrap_int(2))
    #     interpreter.accu = interpreter.space.wrap_int(1)
    #     self.assert_no_branch(interpreter)

    # @bytecode(Instruction.BRANCHINTERVAL, length=-1)
    # def test_branchinterval__yes_1(self, interpreter):
    #     interpreter._push(interpreter.space.wrap_int(1))
    #     interpreter._push(interpreter.space.wrap_int(2))
    #     interpreter.accu = interpreter.space.wrap_int(4)
    #     low = 23
    #     instruct_s(interpreter, low)
    #     hi = 42
    #     instruct_s(interpreter, hi)

    #     interpreter.do_step()
    #     assert interpreter.pc == low + 1

    # @bytecode(Instruction.BRANCHINTERVAL, length=-1)
    # def test_branchinterval__yes_2(self, interpreter):
    #     interpreter._push(interpreter.space.wrap_int(5))
    #     interpreter._push(interpreter.space.wrap_int(2))
    #     interpreter.accu = interpreter.space.wrap_int(4)
    #     low = 23
    #     instruct_s(interpreter, low)
    #     hi = 42
    #     instruct_s(interpreter, hi)

    #     interpreter.do_step()
    #     assert interpreter.pc == hi + 2 + 1

    # @bytecode(Instruction.BRANCHINTERVAL, length=5)
    # def test_branchinterval__no_2(self, interpreter):
    #     cmpr = 3
    #     interpreter._push(interpreter.space.wrap_int(cmpr))
    #     low = 2
    #     interpreter._push(interpreter.space.wrap_int(low))
    #     interpreter.accu = interpreter.space.wrap_int(4)
    #     instruct_s(interpreter, 23)
    #     instruct_s(interpreter, 42)

    #     interpreter.do_step()

    #     assert interpreter.space.unwrap_int(interpreter.accu) == cmpr - low


    # @bytecode(Instruction.C_CALL1, length=3)
    # def test_c_call1(self, interpreter):
    #     val = interpreter.space.wrap_int(1)
    #     other = interpreter.space.wrap_int(2)
    #     interpreter.accu = val

    #     @wrap_primitive(unwrap_spec=[int])
    #     def f(interpreter, arg):
    #         assert arg == 1
    #         assert interpreter.accu == val
    #         return other
    #     num = setup_prim(interpreter, f)
    #     instruct_s(interpreter, num, unsigned=True)
    #     interpreter.do_step()
    #     assert interpreter.accu == other

    # @bytecode(Instruction.C_CALL2, length=3)
    # def test_c_call2(self, interpreter):
    #     val = interpreter.space.wrap_int(1)
    #     other = interpreter.space.wrap_int(2)
    #     interpreter._push(interpreter.space.wrap_int(2))
    #     interpreter.accu = val

    #     @wrap_primitive(unwrap_spec=[int, int])
    #     def f(interpreter, arg1, arg2):
    #         assert arg1 == 1 and arg2 == 2
    #         assert interpreter.accu == val
    #         return other
    #     num = setup_prim(interpreter, f)
    #     instruct_s(interpreter, num, unsigned=True)
    #     interpreter.do_step()
    #     assert interpreter.accu == other

    # @bytecode(Instruction.C_CALL3, length=3)
    # def test_c_call3(self, interpreter):
    #     val = interpreter.space.wrap_int(1)
    #     other = interpreter.space.wrap_int(2)
    #     interpreter._push(interpreter.space.wrap_int(3))
    #     interpreter._push(interpreter.space.wrap_int(2))
    #     interpreter.accu = val

    #     @wrap_primitive(unwrap_spec=[int, int, int])
    #     def f(interpreter, arg1, arg2, arg3):
    #         assert arg1 == 1 and arg2 == 2 and arg3 == 3
    #         assert interpreter.accu == val
    #         return other
    #     num = setup_prim(interpreter, f)
    #     instruct_s(interpreter, num, unsigned=True)
    #     interpreter.do_step()
    #     assert interpreter.accu == other

    # @bytecode(Instruction.C_CALL4, length=3)
    # def test_c_call4(self, interpreter):
    #     val = interpreter.space.wrap_int(1)
    #     other = interpreter.space.wrap_int(2)
    #     interpreter._push(interpreter.space.wrap_int(4))
    #     interpreter._push(interpreter.space.wrap_int(3))
    #     interpreter._push(interpreter.space.wrap_int(2))
    #     interpreter.accu = val

    #     @wrap_primitive(unwrap_spec=[int, int, int, int])
    #     def f(interpreter, arg1, arg2, arg3, arg4):
    #         assert arg1 == 1 and arg2 == 2 and arg3 == 3 and arg4 == 4
    #         assert interpreter.accu == val
    #         return other
    #     num = setup_prim(interpreter, f)
    #     instruct_s(interpreter, num, unsigned=True)
    #     interpreter.do_step()
    #     assert interpreter.accu == other

    # @bytecode(Instruction.C_CALL5, length=3)
    # def test_c_call5(self, interpreter):
    #     val = interpreter.space.wrap_int(1)
    #     other = interpreter.space.wrap_int(2)
    #     interpreter._push(interpreter.space.wrap_int(5))
    #     interpreter._push(interpreter.space.wrap_int(4))
    #     interpreter._push(interpreter.space.wrap_int(3))
    #     interpreter._push(interpreter.space.wrap_int(2))
    #     interpreter.accu = val

    #     @wrap_primitive(unwrap_spec=[int, int, int, int, int])
    #     def f(interpreter, arg1, arg2, arg3, arg4, arg5):
    #         assert (arg1 == 1 and arg2 == 2 and arg3 == 3 and arg4 == 4
    #                 and arg5 == 5)
    #         assert interpreter.accu == val
    #         return other
    #     num = setup_prim(interpreter, f)
    #     instruct_s(interpreter, num, unsigned=True)
    #     interpreter.do_step()
    #     assert interpreter.accu == other

    # @bytecode(Instruction.C_CALLN, length=4)
    # def test_c_calln(self, interpreter):
    #     val = interpreter.space.wrap_int(1)
    #     other = interpreter.space.wrap_int(2)
    #     interpreter._push(interpreter.space.wrap_int(6))
    #     interpreter._push(interpreter.space.wrap_int(5))
    #     interpreter._push(interpreter.space.wrap_int(4))
    #     interpreter._push(interpreter.space.wrap_int(3))
    #     interpreter._push(interpreter.space.wrap_int(2))
    #     interpreter.accu = val

    #     @wrap_primitive()
    #     def f(interpreter, num_args):
    #         assert len(num_args) == 6
    #         assert interpreter.accu == val
    #         return other
    #     num = setup_prim(interpreter, f)
    #     instruct_b(interpreter, 6)
    #     instruct_s(interpreter, num, unsigned=True)
    #     interpreter.do_step()
    #     assert interpreter.accu == other

    # @bytecode(Instruction.MAKEBLOCK, length=5)
    # def test_makeblock(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.MAKEBLOCK1, length=2)
    # def test_makeblock1(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.MAKEBLOCK2, length=2)
    # def test_makeblock2(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.MAKEBLOCK3, length=2)
    # def test_makeblock3(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.MAKEBLOCK4, length=2)
    # def test_makeblock4(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.TAGOF)
    # def test_tagof(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.ACCESS, length=2)
    # def test_access(self, interpreter):
    #     interpreter.env = self._make_env(interpreter)
    #     instruct_b(interpreter, 9)
    #     interpreter.do_step()
    #     assert interpreter.space.unwrap_int(interpreter.accu) == 9

    # @bytecode(Instruction.ACC0)
    # def test_acc0(self, interpreter):
    #     interpreter.env = self._make_env(interpreter)
    #     interpreter.do_step()
    #     assert interpreter.space.unwrap_int(interpreter.accu) == 0

    # @bytecode(Instruction.ACC1)
    # def test_acc1(self, interpreter):
    #     interpreter.env = self._make_env(interpreter)
    #     interpreter.do_step()
    #     assert interpreter.space.unwrap_int(interpreter.accu) == 1

    # @bytecode(Instruction.ACC2)
    # def test_acc2(self, interpreter):
    #     interpreter.env = self._make_env(interpreter)
    #     interpreter.do_step()
    #     assert interpreter.space.unwrap_int(interpreter.accu) == 2

    # @bytecode(Instruction.ACC3)
    # def test_acc3(self, interpreter):
    #     interpreter.env = self._make_env(interpreter)
    #     interpreter.do_step()
    #     assert interpreter.space.unwrap_int(interpreter.accu) == 3

    # @bytecode(Instruction.ACC4)
    # def test_acc4(self, interpreter):
    #     interpreter.env = self._make_env(interpreter)
    #     interpreter.do_step()
    #     assert interpreter.space.unwrap_int(interpreter.accu) == 4

    # @bytecode(Instruction.ACC5)
    # def test_acc5(self, interpreter):
    #     interpreter.env = self._make_env(interpreter)
    #     interpreter.do_step()
    #     assert interpreter.space.unwrap_int(interpreter.accu) == 5

    # @bytecode(Instruction.ATOM, length=2)
    # def test_atom(self, interpreter):
    #     instruct_b(interpreter, 99)
    #     interpreter.do_step()
    #     assert interpreter.accu == interpreter.space.atom(99)

    # @bytecode(Instruction.ATOM0)
    # def test_atom0(self, interpreter):
    #     interpreter.do_step()
    #     assert interpreter.accu == interpreter.space.atom(0)

    # @bytecode(Instruction.ATOM1)
    # def test_atom1(self, interpreter):
    #     interpreter.do_step()
    #     assert interpreter.accu == interpreter.space.atom(1)

    # @bytecode(Instruction.ATOM2)
    # def test_atom2(self, interpreter):
    #     interpreter.do_step()
    #     assert interpreter.accu == interpreter.space.atom(2)

    # @bytecode(Instruction.ATOM3)
    # def test_atom3(self, interpreter):
    #     interpreter.do_step()
    #     assert interpreter.accu == interpreter.space.atom(3)

    # @bytecode(Instruction.ATOM4)
    # def test_atom4(self, interpreter):
    #     interpreter.do_step()
    #     assert interpreter.accu == interpreter.space.atom(4)

    # @bytecode(Instruction.ATOM5)
    # def test_atom5(self, interpreter):
    #     interpreter.do_step()
    #     assert interpreter.accu == interpreter.space.atom(5)

    # @bytecode(Instruction.ATOM6)
    # def test_atom6(self, interpreter):
    #     interpreter.do_step()
    #     assert interpreter.accu == interpreter.space.atom(6)

    # @bytecode(Instruction.ATOM7)
    # def test_atom7(self, interpreter):
    #     interpreter.do_step()
    #     assert interpreter.accu == interpreter.space.atom(7)

    # @bytecode(Instruction.ATOM8)
    # def test_atom8(self, interpreter):
    #     interpreter.do_step()
    #     assert interpreter.accu == interpreter.space.atom(8)

    # @bytecode(Instruction.ATOM9)
    # def test_atom9(self, interpreter):
    #     interpreter.do_step()
    #     assert interpreter.accu == interpreter.space.atom(9)

    # @bytecode(Instruction.GETFIELD, length=2)
    # def test_getfield(self, interpreter):
    #     instruct_b(interpreter, 9)
    #     interpreter.accu = w_constructor(tag("test",10),
    #                                      [integer(i) for i in range(10)])
    #     interpreter.do_step()
    #     assert interpreter.space.unwrap_int(interpreter.accu) == 9

    # @bytecode(Instruction.GETFIELD0)
    # def test_getfield0(self, interpreter):
    #     instruct_b(interpreter, 9)
    #     interpreter.accu = w_constructor(tag("test",10),
    #                                      [integer(i) for i in range(10)])
    #     interpreter.do_step()
    #     assert interpreter.space.unwrap_int(interpreter.accu) == 0

    # @bytecode(Instruction.GETFIELD1)
    # def test_getfield1(self, interpreter):
    #     instruct_b(interpreter, 9)
    #     interpreter.accu = w_constructor(tag("test",10),
    #                                      [integer(i) for i in range(10)])
    #     interpreter.do_step()
    #     assert interpreter.space.unwrap_int(interpreter.accu) == 1

    # @bytecode(Instruction.GETFIELD2)
    # def test_getfield2(self, interpreter):
    #     instruct_b(interpreter, 9)
    #     interpreter.accu = w_constructor(tag("test",10),
    #                                      [integer(i) for i in range(10)])
    #     interpreter.do_step()
    #     assert interpreter.space.unwrap_int(interpreter.accu) == 2

    # @bytecode(Instruction.GETFIELD3)
    # def test_getfield3(self, interpreter):
    #     instruct_b(interpreter, 9)
    #     interpreter.accu = w_constructor(tag("test",10),
    #                                      [integer(i) for i in range(10)])
    #     interpreter.do_step()
    #     assert interpreter.space.unwrap_int(interpreter.accu) == 3

    # @bytecode(Instruction.SETFIELD, length=2)
    # def test_setfield(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.SETFIELD0)
    # def test_setfield0(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.SETFIELD1)
    # def test_setfield1(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.SETFIELD2)
    # def test_setfield2(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.SETFIELD3)
    # def test_setfield3(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.STOP, length=-1)
    # def test_stop(self, interpreter):
    #     val = integer(1)
    #     interpreter.accu = val
    #     try:
    #         interpreter.do_step()
    #     except CamlExit as e:
    #         assert e.code == val

    # @bytecode(Instruction.CHECK_SIGNALS)
    # def test_check_signals(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.APPLY)
    # def test_apply(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.RETURN)
    # def test_return(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.APPTERM)
    # def test_appterm(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.GRAB, length=-1)
    # def test_grab__mark(self, interpreter):
    #     interpreter._push(interpreter.space.mark())

    #     ret_pc = 63
    #     ret_env = interpreter.space.make_env([integer(37)])
    #     retfrm = interpreter.space.make_frame(ret_pc, ret_env)
    #     interpreter._push_ret(retfrm)
    #     interpreter.env = interpreter.space.make_env([integer(3)])

    #     interpreter.do_step()

    #     res = interpreter.accu
    #     assert return_pc(res)._value == 0 + 1
    #     assert return_env(res).get_child(0)._value == 3
    #     assert interpreter.asp._data != interpreter.space.mark()
    #     assert interpreter.pc == ret_pc
    #     assert interpreter.env == ret_env


    # @bytecode(Instruction.GRAB)
    # def test_grab__no_mark(self, interpreter):
    #     interpreter._push(integer(23))
    #     interpreter.env = interpreter.space.make_env([integer(3)])
    #     interpreter.do_step()
    #     assert interpreter.env.get_child(0)._value == 23


    # @bytecode(Instruction.LET)
    # def test_let(self, interpreter):
    #     val = integer(2)
    #     interpreter.accu = val
    #     interpreter.do_step()
    #     assert interpreter.env.get_child(0) == val

    # @bytecode(Instruction.LETREC1, length=3)
    # def test_letrec1(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.DUMMY, length=2)
    # def test_dummy(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.UPDATE)
    # def test_update(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.ENDLET, length=2)
    # def test_endlet(self, interpreter):
    #     interpreter.env = self._make_env(interpreter)
    #     len1 = interpreter.env.get_number_of_children()
    #     instruct_b(interpreter, 9)
    #     interpreter.do_step()
    #     len2 = interpreter.env.get_number_of_children()
    #     assert len1 - len2 == 9

    # @bytecode(Instruction.ENDLET1)
    # def test_endlet1(self, interpreter):
    #     interpreter.env = self._make_env(interpreter)
    #     len1 = interpreter.env.get_number_of_children()
    #     interpreter.do_step()
    #     len2 = interpreter.env.get_number_of_children()
    #     assert len1 - len2 == 1

    # @bytecode(Instruction.PUSHTRAP, length=3)
    # def test_pushtrap(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.RAISE)
    # def test_raise(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.POPTRAP)
    # def test_poptrap(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.PUSH)
    # def test_push(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.POP)
    # def test_pop(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.PUSHMARK)
    # def test_pushmark(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.PUSH_GETGLOBAL_APPLY)
    # def test_push_getglobal_apply(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.PUSH_GETGLOBAL_APPTERM, length=3)
    # def test_push_getglobal_appterm(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.BOOLNOT)
    # def test_boolnot(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.NEGINT)
    # def test_negint(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.SUCCINT)
    # def test_succint(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.PREDINT)
    # def test_predint(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.ADDINT)
    # def test_addint(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.SUBINT)
    # def test_subint(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.MULINT)
    # def test_mulint(self, interpreter):
    #     pytest.skip()
    # @bytecode(Instruction.DIVINT)
    # def test_divint(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.MODINT)
    # def test_modint(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.ANDINT)
    # def test_andint(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.ORINT)
    # def test_orint(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.XORINT)
    # def test_xorint(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.SHIFTLEFTINT)
    # def test_shiftleftint(self, interpreter):
    #     interpreter._push(interpreter.space.wrap_int(2))
    #     interpreter.accu = interpreter.space.wrap_int(3)
    #     interpreter.do_step()
    #     assert interpreter.space.unwrap_int(interpreter.accu) == 3 << 2

    # @bytecode(Instruction.SHIFTRIGHTINTSIGNED)
    # def test_shiftrightintsigned__pos(self, interpreter):
    #     interpreter._push(interpreter.space.wrap_int(2))
    #     interpreter.accu = interpreter.space.wrap_int(13)
    #     interpreter.do_step()
    #     assert interpreter.space.unwrap_int(interpreter.accu) == 13 >> 2

    # @bytecode(Instruction.SHIFTRIGHTINTSIGNED)
    # def test_shiftrightintsigned__neg(self, interpreter):
    #     from rpython.rlib.rarithmetic import r_uint
    #     interpreter._push(interpreter.space.wrap_int(2))
    #     interpreter.accu = interpreter.space.wrap_int(-13)
    #     interpreter.do_step()
    #     assert interpreter.space.unwrap_int(interpreter.accu) == -13 >> 2


    # @bytecode(Instruction.SHIFTRIGHTINTUNSIGNED)
    # def test_shiftrightintunsigned__pos(self, interpreter):
    #     interpreter._push(interpreter.space.wrap_int(2))
    #     interpreter.accu = interpreter.space.wrap_int(13)
    #     interpreter.do_step()
    #     assert interpreter.space.unwrap_int(interpreter.accu) == 13 >> 2

    # @bytecode(Instruction.SHIFTRIGHTINTUNSIGNED)
    # def test_shiftrightintunsigned__pseudo_neg(self, interpreter):
    #     from rpython.rlib.rarithmetic import r_uint
    #     interpreter._push(interpreter.space.wrap_int(2))
    #     interpreter.accu = interpreter.space.wrap_int(r_uint(-13))
    #     interpreter.do_step()
    #     assert interpreter.space.unwrap_int(interpreter.accu) == r_uint(-13) >> 2

    # @bytecode(Instruction.EQ)
    # def test_eq(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.NEQ)
    # def test_neq(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.LTINT)
    # def test_ltint(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.GTINT)
    # def test_gtint(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.LEINT)
    # def test_leint(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.GEINT)
    # def test_geint(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.INCR)
    # def test_incr(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.DECR)
    # def test_decr(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.FLOATOP, length=2)
    # def test_floatop(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.INTOFFLOAT)
    # def test_intoffloat(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.EQFLOAT)
    # def test_eqfloat(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.NEQFLOAT)
    # def test_neqfloat(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.LTFLOAT)
    # def test_ltfloat(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.GTFLOAT)
    # def test_gtfloat(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.LEFLOAT)
    # def test_lefloat(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.GEFLOAT)
    # def test_gefloat(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.STRINGLENGTH)
    # def test_stringlength(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.GETSTRINGCHAR)
    # def test_getstringchar(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.SETSTRINGCHAR)
    # def test_setstringchar(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.EQSTRING)
    # def test_eqstring(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.NEQSTRING)
    # def test_neqstring(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.LTSTRING)
    # def test_ltstring(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.GTSTRING)
    # def test_gtstring(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.LESTRING)
    # def test_lestring(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.GESTRING)
    # def test_gestring(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.MAKEVECTOR)
    # def test_makevector__0(self, interpreter):
    #     interpreter.accu = interpreter.space.wrap_int(0)
    #     interpreter.do_step()
    #     assert interpreter.accu == interpreter.space.atom(0)

    # @bytecode(Instruction.MAKEVECTOR)
    # def test_makevector__n(self, interpreter):
    #     interpreter.accu = interpreter.space.wrap_int(5)
    #     interpreter._push(interpreter.space.wrap_int(1))
    #     interpreter.do_step()
    #     res = interpreter.accu
    #     assert res.get_child(0)._value == 1
    #     assert res.get_child(1)._value == 1
    #     assert res.get_child(2)._value == 1
    #     assert res.get_child(3)._value == 1
    #     assert res.get_child(4)._value == 1

    # @bytecode(Instruction.VECTLENGTH)
    # def test_vectlength(self, interpreter):
    #     exp_len = 10
    #     init = interpreter.space.atom(0)
    #     interpreter.accu = w_constructor(tag("vector", exp_len),
    #                                      [init] * exp_len)
    #     interpreter.do_step()
    #     assert interpreter.space.unwrap_int(interpreter.accu) == exp_len

    # @bytecode(Instruction.GETVECTITEM)
    # def test_getvectitem(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.SETVECTITEM)
    # def test_setvectitem(self, interpreter):
    #     pytest.skip()

    # @bytecode(Instruction.BREAK)
    # def test_break(self, interpreter):
    #     pytest.skip()

    # #
    # # END BYTECODES
    # #
