#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#
# Construction Helper
#
from lambada.model import tag, w_constructor, w_mutablearray, W_Integer

def cons(t, *children):
    ch = list(children)
    return w_constructor(tag(t, len(ch)), ch)

def mcons(t, *children):
    return w_mutablearray(t, list(children))

def integer(n):
    return W_Integer(n)

# EOF
