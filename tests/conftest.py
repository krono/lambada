#!/usr/bin/env python
# -*- coding: utf-8 -*-
import copy
import lambada.space
import lambada.interpreter


def pytest_funcarg__space(request):
    """
    Returns a Lambada object space with all the wrapping functions
    """
    def build_space():
        space = lambada.space.Space()
        space.make_atoms()
        space.initialize_globals([None] * 16)
        return space

    space = request.cached_setup(
            setup=build_space,
            scope="session",
    )

    return copy.deepcopy(space)

def pytest_funcarg__interpreter(request):
    """
    Returns a Lambada interpreter object with a space and the
    instruction as given by the funcarg-value of `b`
    """
    def build_interpreter():
        space = pytest_funcarg__space(request)
        return lambada.interpreter.Interpreter(space, [])

    interpreter = request.cached_setup(
            setup=build_interpreter,
            scope="session",
    )
    instr = request.getfuncargvalue('b')
    print "Testing %s" % instr.name()
    ret = copy.deepcopy(interpreter)
    ret.instructions.append(instr.index())
    return ret
