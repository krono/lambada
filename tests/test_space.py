#!/usr/bin/env python
# -*- coding: utf-8 -*-

import util.debug
from .helper import w_constructor
from struct import pack, unpack
import pytest



class TestSpace(object):

    def test_globals(self, space):
        l = [1,2,3]
        space.initialize_globals(l)
        assert space.get_global(2) == 3
        with pytest.raises(AssertionError):
            space.get_global(3)
        space.set_global(2, 4)
        assert space.get_global(2) == 4
        with pytest.raises(AssertionError):
            space.set_global(3, 4)

    def assert_idempotent(self, space, x):
        assert space.unwrap_int(space.wrap_int(x)) == x

    def test_ints(self, space):
        self.assert_idempotent(space, 0)
        self.assert_idempotent(space, 10)
        self.assert_idempotent(space, 255)
        self.assert_idempotent(space, 256)
        self.assert_idempotent(space, 2**32 - 1)
        self.assert_idempotent(space, 2**32)
        self.assert_idempotent(space, -1)
        self.assert_idempotent(space, -2**32)

    def test_block(self, space):
        items = [space.wrap_int(i) for i in range(3)]
        tag_val = 3
        block = space.make_block(items, tag_val)
        assert space.get_block_tag(block) == tag_val
        assert space.get_block_content(block) == items
