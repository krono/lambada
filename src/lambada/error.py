#!/usr/bin/env python
# -*- coding: utf-8 -*-

class LambadaError(Exception):
    pass
class OCamlError(LambadaError):
    pass


# Data

class DataError(LambadaError):
    pass

class UnwrappingError(DataError):
    pass

class WrappingError(DataError):
    pass

# Bytecode

class CorruptBytecodeError(OCamlError):
    pass

class UnsupportedBytecodeError(OCamlError):
    pass

# Primitives
class UnsupportedPrimitiveError(OCamlError):
    pass
