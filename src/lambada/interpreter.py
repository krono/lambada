#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .object import Object

from .ocaml import primitives
from .ocaml.bytecodes import (Instruction, get_bytecode, InvalidBytecode)
from .ocaml.stack import stack

from rpython.rlib.rstruct.runpack import runpack
from rpython.rlib.objectmodel import we_are_translated, specialize
from rpython.rlib import jit

_indent = [0]
def indent():
    global _indent
    _indent[0] += 1

def outdent():
    global _indent
    _indent[0] -= 1

def _p(string, nl, s, depth):
    depth = _indent[0] if depth is None else depth
    idt = (s * depth)
    if nl:
        print "%s%s" % (idt, string)
    else:
        print "%s%s" % (idt, string),

def p(string="", nl=True, s=".", depth=None):
    if we_are_translated():
        _p(string, nl, s, depth)
    else:
        _p(string, nl, s, depth)
def p2(string=""):
    if not we_are_translated():
        _p(string, nl=True, s=" ", depth=_indent[0]+4)

class Interpreter(Object):
    """
    [from interp.c]
    Registers for the abstract machine

    pc      the code pointer
    sp      the stack pointer
    tp      pointer to the current trap frame
    env     the environment
    accu    the accumulator

    """

    def __init__(self, space, instructions):
        self.space = space
        self.instructions = instructions

        self.pc = 0
        self.accu = self.space.wrap_int(0)
        self.trap_sp = self.sp = stack(self.space.unit())
        self.env = None
        self.extra_args = 0
        self.named_values = {}


    def interprete(self):
        print self.space._global_data
        while True:
            self.do_step()
            print "\t\taccu:", self.accu
            # for i, el in enumerate(self.sp.linearize()):
            #     print "\t\tsp[", i,"]", el
            # print "\t\tenv:", self.env
        assert 0, "unaccessible"

    def do_step(self):
        self.pc = self.step()

    def _get(self, index):
        assert index < len(self.instructions) and index >= 0
        return self.instructions[index]

    # def _getbyte(self, index):
    #     assert index < self.instructions_length() and index >= 0
    #     return runpack("B", self.instructions[index])

    # def _getshort(self, index):
    #     assert index + 2 <= self.instructions_length() and index >= 0
    #     return runpack("h", self.instructions[index:index+2])

    # def _getushort(self, index):
    #     assert index + 2 <= self.instructions_length() and index >= 0
    #     return runpack("H", self.instructions[index:index+2])



    def get_bytecode(self):
        return get_bytecode(self._get(self.pc))

    def _pc_increment(self, current_pc, operand_count):
        return current_pc + operand_count + 1

    def _push(self, what=None):
        if what is None:
            what = self.accu
        self.sp = self.sp.push(what)
        # p2("sp <- %s" % self.sp)

    def _top(self):
        ret, _ = self.sp.pop()
        return ret

    def _pop(self):
        ret, self.sp = self.sp.pop()
        return ret

    def _env_access(self, index):
        # OCaml has the code at elemen 0 (see above)
        # So we decrement the index
        self.accu = self.space.get_closure_variable(self.env, index - 1)
        p2("env access[%d] %s" % (index, self.accu))

    def _getglobal_at_pc(self, pc):
        w_value = self.space.get_global(self._get(pc))
        self.accu = w_value
        # p2("accu <-" + str(self.accu))
        return w_value

    @specialize.arg_or_var(1)
    @jit.unroll_safe
    def _access(self, index):
        _sp = self.sp
        for iter_index in range(index):
            _, _sp = _sp.pop()
        return _sp._data

    def _branch(self):
        _pc = self.pc + 1
        raise CamlJump(_pc + self._get(_pc))

class CamlExit(Exception):
    def __init__(self, code):
        self.code = code

class CamlJump(Exception):
    pc = 0
    _immutable_fields_ = ["pc"]
    def __init__(self, pc):
        self.pc = pc

#
#
#
def bytecode(bcs):
    def _decorate(fn):
        if isinstance(bcs, list):
            # all bytecodes in a list must have the same length, so pick one
            base = bcs[0]
            ops = base.get_operand_count()
            def wrapped(self, b):
                try:
                    fn(self, b, b.index() - base.index())
                except CamlJump as jmp:
                    return jmp.pc
                else:
                    assert ops != -1
                    return self._pc_increment(self.pc, ops)
            for bc in bcs:
                BYTECODE_TABLE[bc.index()] = wrapped
            return wrapped
        else:
            ops = bcs.get_operand_count()
            def wrapped(self, b):
                try:
                    fn(self, b)
                except CamlJump as jmp:
                    return jmp.pc
                else:
                    assert ops != -1
                    return self._pc_increment(self.pc, ops)
            BYTECODE_TABLE[bcs.index()] = wrapped
            return wrapped

    return _decorate

BYTECODE_TABLE = [None] * Instruction.max()

def unwrapped_integer_bytecode(fn):
    def wrapped(self, bc):
        op1 = self.space.unwrap_int(self.accu)
        op2 = self.space.unwrap_int(self._pop())
        ret = fn(self, op1, op2)
        self.accu = self.space.wrap_int(ret)
    return wrapped

def unwrapped_branch_bytecode(fn):
    def wrapped(self, bc):
        tagged_num = self._get(self.pc + 1)
        w_comparand = self.space.wrap_int(tagged_num, tagged=True)
        comparand = self.space.unwrap_int(w_comparand)
        comparee = self.space.unwrap_int(self.accu)
        if fn(self, comparand, comparee):
            self.pc += 1
            self._branch()
    return wrapped

###############################################################################
class __extend__(Interpreter):
    #
    # BEGIN BYTECODES
    #
    # Skeleton generated from instruct.h
    #
    def _acc(self, index):
        self.accu = self._access(index)

    @bytecode([Instruction.ACC0, Instruction.ACC1, Instruction.ACC2,
               Instruction.ACC3, Instruction.ACC4, Instruction.ACC5,
               Instruction.ACC6, Instruction.ACC7])
    def acc_(self, bc, index):
        self._acc(index)

    @bytecode(Instruction.ACC)
    def acc(self, bc):
        index = self._get(self.pc + 1)
        self._acc(index)

    @bytecode([Instruction.PUSH, Instruction.PUSHACC0])
    def push(self, bc, index):
        self._push()

    def _pushacc(self, index):
        self._push()
        self.accu = self._access(index)

    @bytecode([Instruction.PUSHACC1,
               Instruction.PUSHACC2, Instruction.PUSHACC3,
               Instruction.PUSHACC4, Instruction.PUSHACC5,
               Instruction.PUSHACC6, Instruction.PUSHACC7])
    def pushacc_(self, pc, index):
        self._pushacc(index + 1)

    @bytecode(Instruction.PUSHACC)
    def pushacc(self, bc):
        index = self._get(self.pc + 1)
        self._pushacc(index)

    @jit.unroll_safe
    @bytecode(Instruction.POP)
    def pop(self, bc):
        num = self._get(self.pc + 1)
        for _ in range(num):
            self._pop()

    @bytecode(Instruction.ASSIGN)
    def assign(self, bc):
        raise NotImplementedError

    @bytecode([Instruction.ENVACC1, Instruction.ENVACC2, Instruction.ENVACC3,
               Instruction.ENVACC4])
    def envacc_(self, bc, index):
        self._env_access(index + 1)

    @bytecode(Instruction.ENVACC)
    def envacc(self, bc):
        self._env_access(self._get(self.pc + 1))

    @bytecode([Instruction.PUSHENVACC1, Instruction.PUSHENVACC2,
               Instruction.PUSHENVACC3, Instruction.PUSHENVACC4])
    def pushenvacc_(self, bc, index):
        self._push()
        self._env_access(index + 1)

    @bytecode(Instruction.PUSHENVACC)
    def pushenvacc(self, bc):
        self._push()
        self._env_access(self._get(self.pc + 1))

    @bytecode(Instruction.PUSH_RETADDR)
    def push_retaddr(self, bc):
        self._push(self.space.wrap_int(self.extra_args))
        self._push(self.env)
        _pc = self.pc + 1
        self._push(self.space.wrap_int(_pc + self._get(_pc)))

    @bytecode(Instruction.APPLY)
    def apply(self, bc):
        self.extra_args = self._get(self.pc + 1) - 1
        _pc = self.space.get_closure_code(self.accu)
        self.env = self.accu
        raise CamlJump(_pc)

    @jit.unroll_safe
    @bytecode([Instruction.APPLY1, Instruction.APPLY2, Instruction.APPLY3])
    def apply_(self, bc, index):
        w_args = [self._pop() for _ in range(index + 1)]
        self._push(self.space.wrap_int(self.extra_args))
        self._push(self.env)
        self._push(self.space.wrap_int(self.pc + 1))
        for w_arg in w_args:
            self._push(w_arg)
        self.env = self.accu
        self.extra_args = index
        raise CamlJump(self.space.get_closure_code(self.accu))

    @bytecode(Instruction.APPTERM)
    def appterm(self, bc):
        raise NotImplementedError

    @bytecode([Instruction.APPTERM1, Instruction.APPTERM2,
               Instruction.APPTERM3])
    def appterm_(self, bc, index):
        w_args = [self._pop() for _ in range(index + 1)]
        for i in range(self._get(self.pc + 1) - (index + 1)):
            self._pop()
        for w_arg in w_args:
            self._push(w_arg)
        self.extra_args += index
        self.env = self.accu
        raise CamlJump(self.space.get_closure_code(self.accu))

    @jit.unroll_safe
    @bytecode(Instruction.RETURN)
    def return_(self, bc):
        for _ in range(self._get(self.pc + 1)):
            self._pop()
        if self.extra_args > 0:
            self.extra_args -= 1
            self.env = self.accu
            _pc = self.space.get_closure_code(self.accu)
        else:
            _pc = self.space.unwrap_int(self._pop())
            self.env = self._pop()
            self.extra_args = self.space.unwrap_int(self._pop())
        raise CamlJump(_pc)

    @bytecode(Instruction.RESTART)
    def restart(self, bc):
        num_args = self.space.get_closure_size(self.env) - 1
        for i in range(num_args):
            self._push(self.space.get_closure_variable(self.env, i))
        self.env = self.space.get_closure_variable(self.env, 0)
        self.extra_args += num_args

    @bytecode(Instruction.GRAB)
    def grab(self, bc):
        required = self._get(self.pc + 1)
        if self.extra_args >= required:
            self.extra_args -= required
            return

        num_args = self.extra_args + 1
        fields = [self.env] * (num_args + 1)
        for i in range(num_args):
            fields[i + 1] = self._pop()
        self.accu = self.space.make_closure(self.pc + 1 - 3, fields)
        _pc = self.space.unwrap_int(self._pop())
        self.env = self._pop()
        self.extra_args = self.space.unwrap_int(self._pop())
        raise CamlJump(_pc)

    @bytecode(Instruction.CLOSURE)
    def closure(self, bc):
        _pc = self.pc + 1
        nvars = self._get(_pc)
        _pc += 1
        dest = self._get(_pc) + _pc
        if nvars > 0:
            self._push()
        w_vars = [self._pop() for _ in range(nvars)]
        self.accu = self.space.make_closure(dest, w_vars)

    @bytecode(Instruction.CLOSUREREC)
    def closurerec(self, bc):
        _pc = self.pc + 1
        nfuncs = self._get(_pc)
        if nfuncs > 1:
            raise NotImplementedError("no infix stuff yet")
        _pc += 1
        nvars = self._get(_pc)
        _pc += 1
        dest = self._get(_pc) + _pc
        if nvars > 0:
            self._push()
        w_vars = [self._pop() for _ in range(nvars)]
        # pushees = []
        # infix_closure = None
        # for i in range(nfuncs - 1, 0, -1):
        #     infix_closure = self.space.make_infix_block([
        self.accu = self.space.make_closure(dest, w_vars)
        self._push()
        _pc += nfuncs
        raise CamlJump(_pc)

    @bytecode(Instruction.OFFSETCLOSUREM2)
    def offsetclosurem2(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.OFFSETCLOSURE0)
    def offsetclosure0(self, bc):
        self.accu = self.env

    @bytecode(Instruction.OFFSETCLOSURE2)
    def offsetclosure2(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.OFFSETCLOSURE)
    def offsetclosure(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.PUSHOFFSETCLOSUREM2)
    def pushoffsetclosurem2(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.PUSHOFFSETCLOSURE0)
    def pushoffsetclosure0(self, bc):
        self._push()
        self.accu = self.env

    @bytecode(Instruction.PUSHOFFSETCLOSURE2)
    def pushoffsetclosure2(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.PUSHOFFSETCLOSURE)
    def pushoffsetclosure(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.GETGLOBAL)
    def getglobal(self, bc):
        self._getglobal_at_pc(self.pc + 1)

    @bytecode(Instruction.PUSHGETGLOBAL)
    def pushgetglobal(self, bc):
        self._push()
        self._getglobal_at_pc(self.pc + 1)

    def getglobalfield_(self):
        _pc = self.pc + 1
        w_block = self._getglobal_at_pc(_pc)
        self.accu = self.space.get_block_item(w_block, self._get(_pc + 1))

    @bytecode(Instruction.GETGLOBALFIELD)
    def getglobalfield(self, bc):
        self.getglobalfield_()

    @bytecode(Instruction.PUSHGETGLOBALFIELD)
    def pushgetglobalfield(self, bc):
        self._push()
        self.getglobalfield_()

    @bytecode(Instruction.SETGLOBAL)
    def setglobal(self, bc):
        index = self._get(self.pc + 1)
        self.space.set_global(index, self.accu)

    def _atom(self, num):
        self.accu = self.space.atom(num)

    @bytecode(Instruction.ATOM0)
    def atom0(self, bc):
        self._atom(0)

    @bytecode(Instruction.ATOM)
    def atom(self, bc):
        self._atom(self._get(self.pc + 1))

    @bytecode(Instruction.PUSHATOM0)
    def pushatom0(self, bc):
        self._push()
        self._atom(0)

    @bytecode(Instruction.PUSHATOM)
    def pushatom(self, bc):
        self._push()
        self._atom(self._get(self.pc + 1))

    def _makeblock(self, pc, size):
        tag = self._get(pc + 1)
        items = [(self.accu if i == 0 else self._pop()) for i in range(size)]
        self.accu = self.space.make_block(items, tag)

    @bytecode(Instruction.MAKEBLOCK)
    def makeblock(self, bc):
        size = self._get(self.pc + 1)
        self._makeblock(self.pc + 1, size)

    @bytecode([Instruction.MAKEBLOCK1, Instruction.MAKEBLOCK2,
               Instruction.MAKEBLOCK3])
    def makeblock_(self, bc, index):
        # ok, start at 1
        self._makeblock(self.pc, index + 1)

    @bytecode(Instruction.MAKEFLOATBLOCK)
    def makefloatblock(self, bc):
        raise NotImplementedError

    def _getfield(self, index):
        w_value = self.accu
        if self.space.is_block(w_value):
            self.accu = self.space.get_block_item(w_value, index)
        else:
            self.accu = w_value.get_child(index)

    @bytecode([Instruction.GETFIELD0, Instruction.GETFIELD1,
               Instruction.GETFIELD2, Instruction.GETFIELD3])
    def getfield_(self, bc, index):
        self._getfield(index)

    @bytecode(Instruction.GETFIELD)
    def getfield(self, bc):
        self._getfield(self._get(self.pc + 1))

    @bytecode(Instruction.GETFLOATFIELD)
    def getfloatfield(self, bc):
        from .model import W_Float
        w_block = self.accu
        w_value = self.space.get_block_item(w_block, self._get(self.pc + 1))
        assert isinstance(w_value, W_Float)
        self.accu = w_value

    @bytecode([Instruction.SETFIELD0, Instruction.SETFIELD1,
               Instruction.SETFIELD2, Instruction.SETFIELD3])
    def setfield_(self, bc, index):
        raise NotImplementedError

    @bytecode(Instruction.SETFIELD)
    def setfield(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.SETFLOATFIELD)
    def setfloatfield(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.VECTLENGTH)
    def vectlength(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.GETVECTITEM)
    def getvectitem(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.SETVECTITEM)
    def setvectitem(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.GETSTRINGCHAR)
    def getstringchar(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.SETSTRINGCHAR)
    def setstringchar(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.BRANCH)
    def branch(self, bc):
        self._branch()

    @bytecode(Instruction.BRANCHIF)
    def branchif(self, bc):
        from .error import UnwrappingError
        try:
            if self.space.unwrap_int(self.accu) != 0:
                self._branch()
        except UnwrappingError:
            self._branch()

    @bytecode(Instruction.BRANCHIFNOT)
    def branchifnot(self, bc):
        from .error import UnwrappingError
        try:
            if self.space.unwrap_int(self.accu) == 0:
                self._branch()
        except UnwrappingError:
            pass

    @bytecode(Instruction.SWITCH)
    def switch(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.BOOLNOT)
    def boolnot(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.PUSHTRAP)
    def pushtrap(self, bc):
        self._push(self.space.wrap_int(self.extra_args))
        self._push(self.env)
        self._push(self.space.wrap_stack(self.trap_sp))
        _pc = self.pc + 1
        self._push(self.space.wrap_int(_pc + self._get(_pc)))
        self.trap_sp = self.sp

    @bytecode(Instruction.POPTRAP)
    def poptrap(self, bc):
        self._pop()
        self.trap_sp = self.space.unwrap_stack(self._pop())
        self._pop()
        self._pop()

    @bytecode(Instruction.RAISE)
    def raise_(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.CHECK_SIGNALS)
    def check_signals(self, bc):
        raise NotImplementedError

    @jit.look_inside_iff(lambda self, num_args, index, push_accu: \
                         jit.isconstant(num_args) and num_args < 6)
    def _c_call_n(self, num_args, index, push_accu):
        index = self._get(self.pc + 1) if index is -1 else index
        assert index > 0
        assert index < len(primitives.prim_table)
        if push_accu:
            self._push()
        self._push(self.env)

        primitives.prim_table[index](self, num_args,
                                     as_list=True if num_args > 5 else False)

        self.env = self._pop()
        for i in range(num_args - 1): self._pop()

    def _c_call(self, num_args):
        self._c_call_n(num_args, index=-1, push_accu=False)

    @bytecode([Instruction.C_CALL1, Instruction.C_CALL2, Instruction.C_CALL3,
               Instruction.C_CALL4, Instruction.C_CALL5])
    def c_call_(self, bc, index):
        self._c_call(index + 1)

    @bytecode(Instruction.C_CALLN)
    def c_calln(self, bc):
        num_args = self._get(self.pc + 1)
        index = self._get(self.pc + 1 + 1)
        self._c_call_n(num_args, index=index, push_accu=True)

    def _const(self, idx, tagged=False):
        self.accu = self.space.wrap_int(idx, tagged)

    @bytecode([Instruction.CONST0, Instruction.CONST1,
               Instruction.CONST2, Instruction.CONST3])
    def const_(self, bc, index):
        self._const(index)

    @bytecode(Instruction.CONSTINT)
    def constint(self, bc):
        self._const(self._get(self.pc + 1), tagged=True)

    @bytecode([Instruction.PUSHCONST0, Instruction.PUSHCONST1,
               Instruction.PUSHCONST2, Instruction.PUSHCONST3])
    def pushconst_(self, bc, index):
        self._push()
        self._const(index)

    @bytecode(Instruction.PUSHCONSTINT)
    def pushconstint(self, bc):
        self._push()
        self._const(self._get(self.pc + 1), tagged=True)

    @bytecode(Instruction.NEGINT)
    def negint(self, bc):
        self.accu = self.space.wrap_int(- self.space.unwrap_int(self.accu))

    @bytecode(Instruction.ADDINT)
    @unwrapped_integer_bytecode
    def addint(self, op1, op2):
        return op1 + op2

    @bytecode(Instruction.SUBINT)
    @unwrapped_integer_bytecode
    def subint(self, op1, op2):
        return op1 - op2

    @bytecode(Instruction.MULINT)
    @unwrapped_integer_bytecode
    def mulint(self, op1, op2):
        return op1 * op2

    @bytecode(Instruction.DIVINT)
    @unwrapped_integer_bytecode
    def divint(self, op1, op2):
        return op1 / op2

    @bytecode(Instruction.MODINT)
    @unwrapped_integer_bytecode
    def modint(self, op1, op2):
        return op1 % op2

    @bytecode(Instruction.ANDINT)
    @unwrapped_integer_bytecode
    def andint(self, op1, op2):
        return op1 & op2

    @bytecode(Instruction.ORINT)
    @unwrapped_integer_bytecode
    def orint(self, op1, op2):
        return op1 | op2

    @bytecode(Instruction.XORINT)
    @unwrapped_integer_bytecode
    def xorint(self, op1, op2):
        return op1 ^ op2

    @bytecode(Instruction.LSLINT)
    @unwrapped_integer_bytecode
    def lslint(self, op1, op2):
        print op1, " << ", op2, " = ", op1 << op2
        return op1 << op2

    @bytecode(Instruction.LSRINT)
    @unwrapped_integer_bytecode
    def lsrint(self, op1, op2):
        raise NotImplementedError
        # return op1 + op2

    @bytecode(Instruction.ASRINT)
    @unwrapped_integer_bytecode
    def asrint(self, op1, op2):
        raise NotImplementedError
        # return op1 + op2

    @bytecode(Instruction.EQ)
    def eq(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.NEQ)
    def neq(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.LTINT)
    def ltint(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.LEINT)
    def leint(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.GTINT)
    def gtint(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.GEINT)
    def geint(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.OFFSETINT)
    def offsetint(self, bc):
        self.accu = self.space.wrap_int(
            self.space.unwrap_int(self.accu) + self._get(self.pc + 1))

    @bytecode(Instruction.OFFSETREF)
    def offsetref(self, bc):
        increment = self._get(self.pc + 1)
        num_children = self.accu.get_number_of_children()
        assert num_children >= 1
        val = self.space.unwrap_int(self.accu.get_child(0))
        raise NotImplementedError

    @bytecode(Instruction.ISINT)
    def isint(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.GETMETHOD)
    def getmethod(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.BEQ)
    @unwrapped_branch_bytecode
    def beq(self, op1, op2):
        return op1 == op2

    @bytecode(Instruction.BNEQ)
    @unwrapped_branch_bytecode
    def bneq(self, op1, op2):
        return op1 != op2

    @bytecode(Instruction.BLTINT)
    @unwrapped_branch_bytecode
    def bltint(self, op1, op2):
        return op1 < op2

    @bytecode(Instruction.BLEINT)
    @unwrapped_branch_bytecode
    def bleint(self, op1, op2):
        return op1 <= op2

    @bytecode(Instruction.BGTINT)
    @unwrapped_branch_bytecode
    def bgtint(self, op1, op2):
        return op1 > op2

    @bytecode(Instruction.BGEINT)
    @unwrapped_branch_bytecode
    def bgeint(self, op1, op2):
        return op1 >= op2

    @bytecode(Instruction.ULTINT)
    def ultint(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.UGEINT)
    def ugeint(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.BULTINT)
    def bultint(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.BUGEINT)
    def bugeint(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.GETPUBMET)
    def getpubmet(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.GETDYNMET)
    def getdynmet(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.STOP)
    def stop(self, bc):
        raise CamlExit(self.space.unwrap_int(self.accu))

    @bytecode(Instruction.EVENT)
    def event(self, bc):
        raise NotImplementedError

    @bytecode(Instruction.BREAK)
    def break_(self, bc):
        raise NotImplementedError

    #
    # END BYTECODES
    #

###############################################################################

#
# Named values
#
class __extend__(Interpreter):
    def register_named_value(self, name, w_value):
        self.named_values[name] = w_value

    def named_value(self, name):
        try:
            return self.named_values[name]
        except KeyError:
            return None

from rpython.rlib.unroll import unrolling_iterable
unrolling_bcs = unrolling_iterable(enumerate(BYTECODE_TABLE))
def bytecode_step_translated(self):
    bc = self.get_bytecode()
    for index, fn in unrolling_bcs:
        if bc.index() == index:
            return fn(self, bc)
    assert 0, "unreachable"

def bytecode_step_debug(self):
    "NOT_RPYTHON"
    bc = self.get_bytecode()
    opnum = bc.get_operand_count()
    fn = BYTECODE_TABLE[bc.index()]
    fmt = "{0:5d} {1:20s} {2:<30} {3}"
    src = self.pc
    tgt = self.pc + 3 if self.pc + 3 < len(self.instructions) else self.pc
    ops = [str(self.instructions[self.pc + i + 1]) for i in range(opnum)]
    if bc == Instruction.CLOSUREREC:
        nfunc =  self.instructions[src + 1]
        the_bytes = self.instructions[src:tgt + nfunc]
    else:
        the_bytes = self.instructions[src:tgt]
    print fmt.format(self.pc, bc, " ".join(ops), the_bytes)
    return fn(self, bc)

def _step(self):
    if we_are_translated():
        return bytecode_step_translated(self)
    else:
        return bytecode_step_debug(self)
Interpreter.step = _step

def run(space, instructions):
    try:
        Interpreter(space, instructions).interprete()
    except CamlExit as e:
        return e.code
