#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rpython.rlib import jit
from rpython.rlib.unroll import unrolling_iterable

from .object import Object
from .model import (w_constructor, tag,
                    W_Constructor, W_Integer, W_Float,
                    W_String, w_mutablearray, W_MutableArray
)
from .ocaml import tag as ocaml_tag
from .error import UnwrappingError

class Space(Object):
    def __init__(self):
        self.make_atoms()
        self._global_data = None
        self.exe_name = ""
        self.args = []

    def initialize_globals(self, glbls):
        self._global_data = glbls

    def initialize_args(self, exe_name, args):
        self.exe_name = exe_name
        self.args = args

    def set_global(self, index, value):
        assert index < len(self._global_data)
        self._global_data[index] = value

    def get_global(self, index):
        assert index < len(self._global_data)
        return self._global_data[index]

    def make_atoms(self):
        self._atoms = [w_constructor(tag("atom", 0, i), []) for i in range(256)]

    def null_env(self):
        return self.unit()

    def _closure_tag(self):
        return tag("Closure", 2, ocaml_tag.CLOSURE_TAG)

    def make_closure(self, code_start, p_w_vars):
        w_var_block = self.make_block(p_w_vars)
        return w_constructor(self._closure_tag(),
                             [self.wrap_int(code_start), w_var_block])

    def is_closure(self, w_value):
        return (isinstance(w_value, W_Constructor) and
                w_value.get_tag() == self._closure_tag())

    def get_closure_code(self, w_closure):
        assert self.is_closure(w_closure)
        return self.unwrap_int(w_closure.get_child(0))

    def get_closure_variable(self, w_closure, index):
        assert self.is_closure(w_closure)
        return w_closure.get_child(1).get_child(index)

    def get_closure_size(self, w_closure):
        assert self.is_closure(w_closure)
        w_block = w_closure.get_child(1)
        return self.get_block_size(w_block)

    def make_vector(self, num_items, init):
        items = [init] * num_items
        return w_constructor(tag("vector", num_items), items)


    def make_string(self, p_string):
        return W_String(p_string)

    def _block_tag(self, length, tag_):
        return tag("block", length, tag_)

    def make_block(self, items, tag_=0):
        return w_constructor(self._block_tag(len(items), tag_), items)

    def make_infix_block(self, items):
        return self.make_block(ocaml_tag.INFIX_TAG, items)

    def is_block(self, w_value):
        if isinstance(w_value, W_Constructor):
            tag_w = w_value.get_tag()
            return tag_w.name == "block"
        else:
            return False

    def get_block_tag(self, w_block):
        assert self.is_block(w_block)
        w_tag = w_block.get_tag()
        return w_tag.value

    def get_block_content(self, w_block):
        assert self.is_block(w_block)
        w_tag = w_block.get_tag()
        return w_block.get_children()

    def get_block_item(self, w_block, index):
        assert self.is_block(w_block)
        assert index < w_block.get_tag().arity()
        return w_block.get_child(index)

    def get_block_size(self, w_block):
        assert self.is_block(w_block)
        w_tag = w_block.get_tag()
        return w_tag.arity()

    def _make_cons(self, car, cdr):
        return w_constructor(tag("cons", 2), [car, cdr])

    @jit.elidable
    def atom(self, n):
        assert n < 256
        return self._atoms[n]

    @jit.elidable
    def atom_number(self, atom):
        assert self.is_atom(atom)
        return atom.get_tag().value

    def is_atom(self, thing):
        return thing in self._atoms


    @jit.elidable
    def false(self):
        return self.wrap_int(0)

    @jit.elidable
    def true(self):
        return self.wrap_int(1)

    @jit.elidable
    def unit(self):
        return w_constructor(tag("unit", 0), [])

    @jit.elidable
    def is_unit(self, w_value):
        return (isinstance(w_value, W_Constructor) and
                w_value.get_tag() == tag("unit", 0))

# It is boxing day
#
class __extend__(Space):
    def wrap_int(self, number, tagged=False):
        if tagged:
            number >>= 1
        if number >= 0 and number < 256:
            return self.atom(number)
        return W_Integer(number)

    def unwrap_int(self, w_value):
        if self.is_atom(w_value):
            return self.atom_number(w_value)
        if isinstance(w_value, W_Integer):
            return w_value.value()
        raise UnwrappingError("expected W_Integer/Atom, got %s" % (w_value,))

    def unwrap_uint(self, w_value):
        from rpython.rlib.rarithmetic import r_uint
        if isinstance(w_value, W_Integer):
            return r_uint(w_value.value())
        raise UnwrappingError("expected a W_Integer, got %s" % (w_value,))

    def is_tagget_int(self, integer):
        assert isinstance(integer, int)
        return (integer & 1) == 1

    def wrap_float(self, value):
        assert isinstance(value, float)
        return W_Float(value)

    def unwrap_float(self, w_value):
        assert isinstance(w_value, W_Float)
        return w_value.value()

    def wrap_string(self, value):
        assert isinstance(value, str)
        return W_String(value)

    def unwrap_string(self, w_value):
        assert isinstance(w_value, W_String)
        return w_value.value()

    def unwrap_char(self, w_value):
        raise NotImplementedError("muhahaha")

    def is_list_element(self, w_value):
        if isinstance(w_value, W_Constructor):
            tag_w = w_value.get_tag()
            return (self.is_unit(w_value) or
                    (tag_w.name == "cons" and tag_w.arity() == 2))
        else:
            return False

    def is_list(self, w_value):
        return (self.is_list_element(w_value) and
                self.is_list(w_value.get_child(1)))

    def wrap_list(self, p_list, size=-1):
        w_result = self.unit()
        if size == -1:
            for w_element in reversed(p_list):
                w_result = self._make_cons(w_element, w_result)
        else:
            for i in range(size):
                w_result = self._make_cons(p_list[i], w_result)
        return w_result

    def unwrap_list(self, w_value, size=-1):
        if size == -1:
            p_list = []
            while not self.is_unit(w_value):
                assert self.is_list_element(w_value)
                p_list.append(w_value.get_child(0))
                w_value = w_value.get_child(1)
        else:
            p_list = [None] * size
            for i in range(size):
                assert self.is_list_element(w_value)
                p_list[i] = w_value.get_child(0)
                w_value = w_value.get_child(1)
        return p_list


    def is_array(self, w_value):
        return isinstance(w_value, W_MutableArray)

    def wrap_array(self, p_array, tag_name):
        return w_mutablearray(tag_name, p_array)

    def unwrap_array(self, w_value):
        assert self.is_array(w_value)
        return (w_value.get_tag(), w_value.get_children())

    def wrap_stack(self, s_value):
        from .ocaml.stack import OCamlStackElement, W_WrappedStack
        assert isinstance(s_value, OCamlStackElement)
        return W_WrappedStack(s_value)

    def unwrap_stack(self, w_value):
        from .ocaml.stack import W_WrappedStack
        assert isinstance(w_value, W_WrappedStack)
        return w_value.sp()
