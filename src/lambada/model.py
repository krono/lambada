#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Hi.
#
"""

  W_Value
    W_Tag
    W_Integer
    W_Float
    W_String
    W_Constructor
      W_NAryConstructor
    W_Lambda

"""
from rpython.rlib import jit
from rpython.rlib.unroll import unrolling_iterable
from rpython.rlib.objectmodel import compute_identity_hash, r_dict
from rpython.rlib.debug import debug_start, debug_stop, debug_print

from lambada.object import Object
from lambada.shape import CompoundShape, in_storage_shape, default_shape

class W_Value(Object):
    _attrs_ = []
    def shape(self):
        return in_storage_shape


class W_Tag(W_Value):
    tags = {}

    # _immutable_fields_ = ['name', 'value', '_arity', '_cursor', 'default_shape']
    _immutable_fields_ = ['name', 'value', '_arity', 'default_shape']

    def __init__(self, name, arity, value):
        # from lambada.expression import W_ConstructorCursor
        self.name = name
        self.value = value
        self._arity = arity
        # self._cursor = W_ConstructorCursor(self)
        self.default_shape = default_shape(self, arity)

    def arity(self):
        return self._arity

    #
    # Tags compare by identity
    #
    def __eq__(self, other): #pragma: no cover
        return self is other


# we override that..
def tag(name, arity, value=-1):
    assert isinstance(name, str)
    assert isinstance(arity, int)
    if value < 0:
        n_value = 0
    else:
        n_value = value
    assert n_value >= 0
    tag_ = (name, arity, n_value)
    w_tag = W_Tag.tags.get(tag_ , None)
    if w_tag is None:
        w_tag = W_Tag(name, arity, n_value)
        W_Tag.tags[tag_] = w_tag

    assert isinstance(w_tag, W_Tag)
    return w_tag

class W_Integer(W_Value):
    _immutable_fields_ = ['_int_value']
    def __init__(self, value):
        self._int_value = value
    def value(self):
        return self._int_value

class W_String(W_Value):
    _immutable_fields_ = ['_str_value']
    def __init__(self, value):
        self._str_value = value
    def value(self):
        return self._str_value

class W_Float(W_Value):
    _immutable_fields_ = ['_float_value']
    def __init__(self, value):
        self._float_value = value
    def value(self):
        return self._float_value


class W_Constructor(W_Value):

    _immutable_fields_ = ['_shape']

    def __init__(self, shape):
        assert isinstance(shape, CompoundShape)
        self._shape = shape

    def _init_storage(self, stroage):
        pass

    def get_storage(self):
        return []

    def get_storage_at(self, index):
        raise IndexError()

    def get_storage_width(self):
        return 0

    def get_tag(self):
        return self.shape()._tag

    def get_children(self):
        return self.shape().get_children(self)

    def get_child(self, index):
        return self.shape().get_child(self, index)

    def get_number_of_children(self):
        return self.shape().get_number_of_direct_children()

    def shape(self):
        return jit.promote(self._shape)

    def __eq__(self, other):
        if isinstance(other, W_Constructor):
            if self.get_number_of_children() == other.get_number_of_children():
                return self.get_children() == other.get_children()
        return False

class W_NAryConstructor(W_Constructor):

    _immutable_fields_ = ['_storage[*]']

    def _init_storage(self, storage):
        self._storage = storage or []

    def get_storage(self):
        return self._storage

    def get_storage_at(self, index):
        return self._storage[index]

    def get_storage_width(self):
        return len(self._storage)

STORAGE_ATTR_TEMPLATE = "storage_%d"

def constructor_class_name(n_storage):
    return 'W_Constructor%d' % n_storage


def generate_constructor_class(n_storage):

    storage_iter = unrolling_iterable(range(n_storage))

    class constructor_class(W_Constructor):
        _immutable_fields_ = [(STORAGE_ATTR_TEMPLATE % x) for x in storage_iter]

        def _init_storage(self, storage):
            for x in storage_iter:
                setattr(self, STORAGE_ATTR_TEMPLATE % x, storage[x])

        def get_storage(self):
            result = [None] * n_storage
            for x in storage_iter:
                result[x] = getattr(self, STORAGE_ATTR_TEMPLATE % x)
            return result

        def get_storage_at(self, index):
            for x in storage_iter:
                if x == index:
                    return getattr(self, STORAGE_ATTR_TEMPLATE % x)
            raise IndexError

        def get_storage_width(self):
            return n_storage

    constructor_class.__name__ = constructor_class_name(n_storage)
    return constructor_class

constructor_classes = [W_Constructor]
for n_storage in range(1, 10):
    constructor_classes.append(generate_constructor_class(n_storage))

class_iter = unrolling_iterable(enumerate(constructor_classes))

def select_constructor_class(storage):
    length = len(storage)
    for i, cls in class_iter:
        if i == length:
            return cls
    # otherwise:
    return W_NAryConstructor


def prepare_constructor(tag, children):
    """
    create what is necessary to build a constructor.
    """
    pre_shape = tag.default_shape
    shape, storage = pre_shape.fusion(children)
    return (shape, storage)

def w_constructor(tag, children):
    shape, storage = prepare_constructor(tag, children)
    constr_cls = select_constructor_class(storage)
    constr = constr_cls(shape)
    constr._init_storage(storage)
    return constr


class W_MutableArray(W_Value):

    _immutable_fields_ = ['tag']

    def __init__(self, tag, storage):
        assert len(storage) == tag.arity()
        self.tag = tag
        self._storage = storage

    def get_tag(self):
        return self.tag

    def get_storage(self):
        return self._storage

    def get_storage_at(self, index):
        return self._storage[index]

    def get_storage_width(self):
        return self.tag.arity()

    def get_children(self):
        return self.get_storage()

    def get_child(self, index):
        return self.get_storage_at(index)

    #
    # mutating
    #
    def set_child(self, index, w_value):
        self.set_storage_at(index, w_value)

    def set_storage_at(self, index, w_value):
        self._storage[index] = w_value

    def get_number_of_children(self):
        return len(self._storage)

    def __eq__(self, other):
        if isinstance(other, W_MutableArray):
            if self.get_number_of_children() == other.get_number_of_children():
                return self.get_children() == other.get_children()
        return False

def w_mutablearray(tag_name, children):
    return W_MutableArray(tag(tag_name, len(children)), children)

# class W_Lambda(W_Value):
#     """
#     λ arity is the number of patterns in the first rule, or zero
#     """

#     _immutable_fields_ = ['_rules[*]', '_cursor']

#     def __init__(self, rules, name=""):
#         from lambada.expression import W_LambdaCursor
#         self._rules = rules
#         self._name = name
#         self._cursor = W_LambdaCursor(self)

#     @jit.elidable
#     def arity(self):
#         assert len(self._rules) > 0
#         return self._rules[0].arity()


#     def name(self):
#         if len(self._name) > 0:
#             return unicode(self._name)
#         else:
#             return who(self)
# EOF
