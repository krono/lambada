#!/usr/bin/env python
# -*- coding: utf-8 -*-

from lambada.object import Object

from util.repr import urepr, uni

class StackElement(Object):

    _attrs_ = ['_data', '_next']

    def __init__(self, data=None, next=None):
        self._data = data
        self._next = next

class ExecutionStackElement(StackElement):
    pass

class OperandStackElement(StackElement):
    pass

class Stack(Object):
    def __init__(self, ex, op):
        self.execution_stack = ex
        self.operand_stack = op
