#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
# from mlvalues.h
#
#/* 1- If tag < No_scan_tag : a tuple of fields.  */
#/* 2- If tag >= No_scan_tag : a sequence of bytes. */
NUM_TAGS         = (1 << 8)
CLOSURE_TAG      = 247
LAZY_TAG         = 246
OBJECT_TAG       = 248
INFIX_TAG        = 249
FORWARD_TAG      = 250
#
NO_SCAN_TAG      = 251
#
ABSTRACT_TAG     = 251
STRING_TAG       = 252
DOUBLE_TAG       = 253
DOUBLE_ARRAY_TAG = 254
CUSTOM_TAG       = 255

#
