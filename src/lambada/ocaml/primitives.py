#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
# Adapted from SPy/RSqueakVM
#
from . import io
from .. import model
from ..error import UnsupportedPrimitiveError

from rpython.rlib import rarithmetic, rfloat, unroll, jit, rstring
from rpython.rlib.longlong2float import float2longlong
from rpython.rlib.longlong2float import longlong2float

NUM_PRIMS=283

# def assert_bounds(n0, minimum, maximum):
#     if not minimum <= n0 < maximum:
#         raise PrimitiveFailedError()

# ___________________________________________________________________________
# Primitive table: it is filled in at initialization time with the
# primitive functions.  Each primitive function takes two
# arguments, an interpreter and an argument_count
# completes, and returns a result, by default the 0-Atom
def make_noop(code):
    def _default(interpreter, s_argument_count):
        raise NotImplementedError("missing")
    return _default

prim_table = [make_noop(i) for i in range(NUM_PRIMS)]

class PrimitiveHolder(object):
    _immutable_fields_ = ["prim_table[*]"]

prim_holder = PrimitiveHolder()
prim_holder.prim_table = prim_table
# clean up namespace:
del i
prim_table_implemented_only = []

value = object()
char = object()
channel = object()
unit = object()
handle = object()

def expose_primitive(code, name, unwrap_spec=None, may_context_switch=True):
    def decorator(func):
        assert code not in prim_table
        func.func_name = "prim_" + name

        # expose the name
        globals()[name.upper()] = code

        wrapped = wrap_primitive(
            unwrap_spec=unwrap_spec, may_context_switch=may_context_switch,
        )(func)
        wrapped.func_name = "wrap_prim_" + name
        prim_table[code] = wrapped
        prim_table_implemented_only.append((code, wrapped))
        return func
    return decorator


def wrap_primitive(unwrap_spec=None, may_context_switch=True):
    "NOT_RPYTHON"
    # some serious magic, don't look
    import inspect
    from rpython.rlib.unroll import unrolling_iterable

    assert unwrap_spec is None or unwrap_spec

    def decorator(func):
        if unwrap_spec is None:
            def wrapped(interpreter, argument_count, as_list=False):
                assert as_list
                # if as_list:
                #env is top on on stack...
                _env, sp = interpreter.sp.pop()
                args = [None] * argument_count
                for i in range(argument_count):
                    args[i], sp = sp.pop()
                w_result = func(interpreter, args)
                # else:
                #     w_result = func(interpreter, argument_count)
                if w_result is None:
                    interpreter.accu = interpreter.space.unit()
                else:
                    assert isinstance(w_result, model.W_Value)
                    interpreter.accu = w_result
            return wrapped

        # unwrap_spec not None.
        len_unwrap_spec = len(unwrap_spec)
        actual_arglen = len(inspect.getargspec(func)[0]) - 1

        if len_unwrap_spec == 1 and unwrap_spec[0] == unit:
            # Arg-less primitive
            def wrapped(interpreter, argument_count, as_list=False):
                assert argument_count == 1
                assert as_list == False
                w_result = func(interpreter)
                if w_result is not None:
                    assert isinstance(w_result, model.W_Value)
                    interpreter.accu = w_result
            return wrapped

        assert (len_unwrap_spec == actual_arglen), \
            "wrong number of unwrap arguments (%d for %d) in %r" % (
                len_unwrap_spec, actual_arglen, func)
        unrolling_unwrap_spec = unrolling_iterable(enumerate(unwrap_spec))
        def wrapped(interpreter, argument_count, as_list=False):
            assert as_list == False
            assert argument_count == len_unwrap_spec
            assert argument_count <= 5
            args = ()
            #env is top on on stack...
            _env, sp = interpreter.sp.pop()
            for i, spec in unrolling_unwrap_spec:
                if i == 0:
                    w_arg = interpreter.accu
                else:
                    w_arg, sp = sp.pop()
                if False: pass
                elif spec is value:
                    assert isinstance(w_arg, model.W_Value)
                    args += (w_arg, )
                elif spec is int:
                    assert (isinstance(w_arg, model.W_Integer) or
                            interpreter.space.is_atom(w_arg))
                    args += (interpreter.space.unwrap_int(w_arg), )
                elif spec is float:
                    assert isinstance(w_arg, model.W_Float)
                    args += (interpreter.space.unwrap_float(w_arg), )
                elif spec is str:
                    assert isinstance(w_arg, model.W_String)
                    args += (interpreter.space.unwrap_string(w_arg), )
                elif spec is list:
                    assert isinstance(w_arg, model.W_Constructor)
                    args += (interpreter.space.unwrap_array(w_arg), )
                elif spec is channel:
                    assert isinstance(w_arg, io.W_Channel)
                    args += (w_arg.unwrapped_fd(), )
                # elif spec is char:
                #     args += (interpreter.space.unwrap_char(w_arg), )
                elif spec is unit:
                    args += (w_arg, )
                else:
                    raise NotImplementedError(
                        "unknown unwrap_spec %s" % (spec, ))
            w_result = func(interpreter, *args)
            if w_result is None:
                interpreter.accu = interpreter.space.unit()
            else:
                assert isinstance(w_result, model.W_Value)
                interpreter.accu = w_result
        return wrapped
    return decorator

@expose_primitive(0, "caml_abs_float", unwrap_spec=[float])
def func(interpreter, f):
    raise NotImplementedError("missing")

@expose_primitive(1, "caml_acos_float", unwrap_spec=[float])
def func(interpreter, f):
    raise NotImplementedError("missing")

@expose_primitive(2, "caml_add_float", unwrap_spec=[float, float])
def func(interpreter, f, g):
    raise NotImplementedError("missing")

@expose_primitive(3, "caml_alloc_dummy", unwrap_spec=[int])
def func(interpreter, size):
    raise NotImplementedError("missing")

@expose_primitive(4, "caml_alloc_dummy_float", unwrap_spec=[int])
def func(interpreter, size):
    raise NotImplementedError("missing")

@expose_primitive(5, "caml_array_append", unwrap_spec=[list, list])
def func(interpreter, a1, a2):
    raise NotImplementedError("missing")

@expose_primitive(6, "caml_array_blit", unwrap_spec=[list, int, list, int, int])
def func(interpreter, a1, ofs1, a2, ofs2, n):
    raise NotImplementedError("missing")

@expose_primitive(7, "caml_array_concat", unwrap_spec=[list])
def func(interpreter, al):
    raise NotImplementedError("missing")

@expose_primitive(8, "caml_array_get", unwrap_spec=[value, int])
def func(interpreter, array, index):
    raise NotImplementedError("missing")

@expose_primitive(9, "caml_array_get_addr", unwrap_spec=[value, int])
def func(interpreter, w_array, index):
    if isinstance(w_array, model.W_MutableArray):
        return w_array.get_child(index)
    else:
        assert interpreter.space.is_block(w_array)
        return interpreter.space.get_block_item(w_array, index - 1)

@expose_primitive(10, "caml_array_get_float", unwrap_spec=[value, int])
def func(interpreter, array, index):
    raise NotImplementedError("missing")

@expose_primitive(11, "caml_array_set", unwrap_spec=[list, int, value])
def func(interpreter, array, index, newval):
    raise NotImplementedError("missing")

@expose_primitive(12, "caml_array_set_addr", unwrap_spec=[list, int, value])
def func(interpreter, array, index, newval):
    raise NotImplementedError("missing")

@expose_primitive(13, "caml_array_set_float", unwrap_spec=[list, int, value])
def func(interpreter, array, index, newval):
    raise NotImplementedError("missing")

@expose_primitive(14, "caml_array_sub", unwrap_spec=[list, int, int])
def func(interpreter, a, ofs, length):
    raise NotImplementedError("missing")

@expose_primitive(15, "caml_array_unsafe_get", unwrap_spec=[list, int])
def func(interpreter, array, index):
    raise NotImplementedError("missing")

@expose_primitive(16, "caml_array_unsafe_get_float", unwrap_spec=[list, int])
def func(interpreter, array, index):
    raise NotImplementedError("missing")

@expose_primitive(17, "caml_array_unsafe_set", unwrap_spec=[list, int, value])
def func(interpreter, array, index, newval):
    raise NotImplementedError("missing")

@expose_primitive(18, "caml_array_unsafe_set_addr", unwrap_spec=[list, int, value])
def func(interpreter, array, index, newval):
    raise NotImplementedError("missing")

@expose_primitive(19, "caml_array_unsafe_set_float", unwrap_spec=[list, int, value])
def func(interpreter, array, index, newval):
    raise NotImplementedError("missing")

@expose_primitive(20, "caml_asin_float", unwrap_spec=[float])
def func(interpreter, f):
    raise NotImplementedError("missing")

@expose_primitive(21, "caml_atan2_float", unwrap_spec=[float, float])
def func(interpreter, f, g):
    raise NotImplementedError("missing")

@expose_primitive(22, "caml_atan_float", unwrap_spec=[float])
def func(interpreter, f):
    raise NotImplementedError("missing")

@expose_primitive(23, "caml_backtrace_status", unwrap_spec=[unit])
def func(interpreter):
    raise NotImplementedError("missing")

@expose_primitive(24, "caml_bitvect_test", unwrap_spec=[value, int])
def func(interpreter, bv, n):
    raise NotImplementedError("missing")

@expose_primitive(25, "caml_blit_string", unwrap_spec=[str, int, str, int, int])
def func(interpreter, s1, ofs1, s2, ofs2, n):
    raise NotImplementedError("missing")

@expose_primitive(26, "caml_bswap16", unwrap_spec=[value])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(27, "caml_ceil_float", unwrap_spec=[float])
def func(interpreter, f):
    raise NotImplementedError("missing")

@expose_primitive(28, "caml_channel_descriptor", unwrap_spec=[channel])
def func(interpreter, vchannel):
    raise NotImplementedError("missing")

@expose_primitive(29, "caml_classify_float", unwrap_spec=[float])
def func(interpreter, vd):
    raise NotImplementedError("missing")

@expose_primitive(30, "caml_compare", unwrap_spec=[value, value])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(31, "caml_convert_raw_backtrace", unwrap_spec=[value])
def func(interpreter, backtrace):
    raise NotImplementedError("missing")

@expose_primitive(32, "caml_copysign_float", unwrap_spec=[float, float])
def func(interpreter, f, g):
    raise NotImplementedError("missing")

@expose_primitive(33, "caml_cos_float", unwrap_spec=[float])
def func(interpreter, f):
    raise NotImplementedError("missing")

@expose_primitive(34, "caml_cosh_float", unwrap_spec=[float])
def func(interpreter, f):
    raise NotImplementedError("missing")

@expose_primitive(35, "caml_create_string", unwrap_spec=[int])
def func(interpreter, length):
    raise NotImplementedError("missing")

@expose_primitive(36, "caml_div_float", unwrap_spec=[float, float])
def func(interpreter, f, g):
    raise NotImplementedError("missing")

@expose_primitive(37, "caml_dynlink_add_primitive", unwrap_spec=[handle])
def func(interpreter, handle):
    raise NotImplementedError("missing")

@expose_primitive(38, "caml_dynlink_close_lib", unwrap_spec=[handle])
def func(interpreter, handle):
    raise NotImplementedError("missing")

@expose_primitive(39, "caml_dynlink_get_current_libs", unwrap_spec=[unit])
def func(interpreter):
    raise NotImplementedError("missing")

@expose_primitive(40, "caml_dynlink_lookup_symbol", unwrap_spec=[handle, str])
def func(interpreter, handle, symbolname):
    raise NotImplementedError("missing")

@expose_primitive(41, "caml_dynlink_open_lib", unwrap_spec=[str, str])
def func(interpreter, mode, filename):
    raise NotImplementedError("missing")

@expose_primitive(42, "caml_ensure_stack_capacity", unwrap_spec=[int])
def func(interpreter, required_space):
    raise NotImplementedError("missing")

@expose_primitive(43, "caml_eq_float", unwrap_spec=[float, float])
def func(interpreter, f, g):
    raise NotImplementedError("missing")

@expose_primitive(44, "caml_equal", unwrap_spec=[value, value])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(45, "caml_exp_float", unwrap_spec=[float])
def func(interpreter, f):
    raise NotImplementedError("missing")

@expose_primitive(46, "caml_expm1_float", unwrap_spec=[float])
def func(interpreter, f):
    raise NotImplementedError("missing")

@expose_primitive(47, "caml_fill_string", unwrap_spec=[str, int, int, int])
def func(interpreter, s, offset, length, init):
    raise NotImplementedError("missing")

@expose_primitive(48, "caml_final_register", unwrap_spec=[float, value])
def func(interpreter, f, v):
    raise NotImplementedError("missing")

@expose_primitive(49, "caml_final_release", unwrap_spec=[unit])
def func(interpreter):
    raise NotImplementedError("missing")

@expose_primitive(50, "caml_float_compare", unwrap_spec=[float, float])
def func(interpreter, vf, vg):
    raise NotImplementedError("missing")

@expose_primitive(51, "caml_float_of_int", unwrap_spec=[int])
def func(interpreter, n):
    raise NotImplementedError("missing")

@expose_primitive(52, "caml_float_of_string", unwrap_spec=[str])
def func(interpreter, vs):
    raise NotImplementedError("missing")

@expose_primitive(53, "caml_floor_float", unwrap_spec=[float])
def func(interpreter, f):
    raise NotImplementedError("missing")

@expose_primitive(54, "caml_fmod_float", unwrap_spec=[float, float])
def func(interpreter, f1, f2):
    raise NotImplementedError("missing")

@expose_primitive(55, "caml_format_float", unwrap_spec=[str, float])
def func(interpreter, fmt, arg):
    raise NotImplementedError("missing")

@expose_primitive(56, "caml_format_int", unwrap_spec=[str, int])
def func(interpreter, fmt, arg):
    raise NotImplementedError("missing")

@expose_primitive(57, "caml_frexp_float", unwrap_spec=[float])
def func(interpreter, f):
    raise NotImplementedError("missing")

@expose_primitive(58, "caml_gc_compaction", unwrap_spec=[value])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(59, "caml_gc_counters", unwrap_spec=[value])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(60, "caml_gc_full_major", unwrap_spec=[value])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(61, "caml_gc_get", unwrap_spec=[value])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(62, "caml_gc_major", unwrap_spec=[value])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(63, "caml_gc_major_slice", unwrap_spec=[value])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(64, "caml_gc_minor", unwrap_spec=[value])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(65, "caml_gc_quick_stat", unwrap_spec=[value])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(66, "caml_gc_set", unwrap_spec=[value])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(67, "caml_gc_stat", unwrap_spec=[value])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(68, "caml_ge_float", unwrap_spec=[float, float])
def func(interpreter, f, g):
    raise NotImplementedError("missing")

@expose_primitive(69, "caml_get_current_callstack", unwrap_spec=[int])
def func(interpreter, max_frames_value):
    raise NotImplementedError("missing")

@expose_primitive(70, "caml_get_current_environment", unwrap_spec=[unit])
def func(interpreter):
    raise NotImplementedError("missing")

@expose_primitive(71, "caml_get_exception_backtrace", unwrap_spec=[unit])
def func(interpreter):
    raise NotImplementedError("missing")

@expose_primitive(72, "caml_get_exception_raw_backtrace", unwrap_spec=[unit])
def func(interpreter):
    raise NotImplementedError("missing")

@expose_primitive(73, "caml_get_global_data", unwrap_spec=[unit])
def func(interpreter):
    raise NotImplementedError("missing")

@expose_primitive(74, "caml_get_public_method", unwrap_spec=[object, value])
def func(interpreter, obj, tag):
    raise NotImplementedError("missing")

@expose_primitive(75, "caml_get_section_table", unwrap_spec=[unit])
def func(interpreter):
    raise NotImplementedError("missing")

@expose_primitive(76, "caml_greaterequal", unwrap_spec=[value, value])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(77, "caml_greaterthan", unwrap_spec=[value, value])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(78, "caml_gt_float", unwrap_spec=[float, float])
def func(interpreter, f, g):
    raise NotImplementedError("missing")

@expose_primitive(79, "caml_hash", unwrap_spec=[int, int, int, object])
def func(interpreter, count, limit, seed, obj):
    raise NotImplementedError("missing")

@expose_primitive(80, "caml_hash_univ_param", unwrap_spec=[int, int, object])
def func(interpreter, count, limit, obj):
    raise NotImplementedError("missing")

@expose_primitive(81, "caml_hypot_float", unwrap_spec=[float, float])
def func(interpreter, f, g):
    raise NotImplementedError("missing")

@expose_primitive(82, "caml_input_value", unwrap_spec=[channel])
def func(interpreter, vchan):
    raise NotImplementedError("missing")

@expose_primitive(83, "caml_input_value_from_string", unwrap_spec=[str, int])
def func(interpreter, str, ofs):
    raise NotImplementedError("missing")

@expose_primitive(84, "caml_install_signal_handler", unwrap_spec=[int, int])
def func(interpreter, signal_number, action):
    raise NotImplementedError("missing")

@expose_primitive(85, "caml_int32_add", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(86, "caml_int32_and", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(87, "caml_int32_bits_of_float", unwrap_spec=[float])
def func(interpreter, vd):
    raise NotImplementedError("missing")

@expose_primitive(88, "caml_int32_bswap", unwrap_spec=[int])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(89, "caml_int32_compare", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(90, "caml_int32_div", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(91, "caml_int32_float_of_bits", unwrap_spec=[int])
def func(interpreter, vi):
    raise NotImplementedError("missing")

@expose_primitive(92, "caml_int32_format", unwrap_spec=[str, int])
def func(interpreter, fmt, arg):
    raise NotImplementedError("missing")

@expose_primitive(93, "caml_int32_mod", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(94, "caml_int32_mul", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(95, "caml_int32_neg", unwrap_spec=[int])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(96, "caml_int32_of_float", unwrap_spec=[float])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(97, "caml_int32_of_int", unwrap_spec=[int])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(98, "caml_int32_of_string", unwrap_spec=[str])
def func(interpreter, s):
    raise NotImplementedError("missing")

@expose_primitive(99, "caml_int32_or", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(100, "caml_int32_shift_left", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(101, "caml_int32_shift_right", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(102, "caml_int32_shift_right_unsigned", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(103, "caml_int32_sub", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(104, "caml_int32_to_float", unwrap_spec=[int])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(105, "caml_int32_to_int", unwrap_spec=[int])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(106, "caml_int32_xor", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(107, "caml_int64_add", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(108, "caml_int64_and", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(109, "caml_int64_bits_of_float", unwrap_spec=[float])
def func(interpreter, vd):
    raise NotImplementedError("missing")

@expose_primitive(110, "caml_int64_bswap", unwrap_spec=[int])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(111, "caml_int64_compare", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(112, "caml_int64_div", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(113, "caml_int64_float_of_bits", unwrap_spec=[int])
def func(interpreter, vi):
    return interpreter.space.wrap_float(
        longlong2float(vi))

@expose_primitive(114, "caml_int64_format", unwrap_spec=[str, int])
def func(interpreter, fmt, arg):
    raise NotImplementedError("missing")

@expose_primitive(115, "caml_int64_mod", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(116, "caml_int64_mul", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(117, "caml_int64_neg", unwrap_spec=[int])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(118, "caml_int64_of_float", unwrap_spec=[float])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(119, "caml_int64_of_int", unwrap_spec=[int])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(120, "caml_int64_of_int32", unwrap_spec=[int])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(121, "caml_int64_of_nativeint", unwrap_spec=[int])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(122, "caml_int64_of_string", unwrap_spec=[str])
def func(interpreter, s):
    raise NotImplementedError("missing")

@expose_primitive(123, "caml_int64_or", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(124, "caml_int64_shift_left", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(125, "caml_int64_shift_right", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(126, "caml_int64_shift_right_unsigned", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(127, "caml_int64_sub", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(128, "caml_int64_to_float", unwrap_spec=[int])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(129, "caml_int64_to_int", unwrap_spec=[int])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(130, "caml_int64_to_int32", unwrap_spec=[int])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(131, "caml_int64_to_nativeint", unwrap_spec=[int])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(132, "caml_int64_xor", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(133, "caml_int_compare", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(134, "caml_int_of_float", unwrap_spec=[float])
def func(interpreter, f):
    raise NotImplementedError("missing")

@expose_primitive(135, "caml_int_of_string", unwrap_spec=[str])
def func(interpreter, s):
    return interpreter.space.wrap_int(rarithmetic.string_to_int(
        rstring.replace(s, "_", ""), base=0))

@expose_primitive(136, "caml_invoke_traced_function", unwrap_spec=[value, value, value])
def func(interpreter, codeptr, env, arg):
    raise NotImplementedError("missing")

@expose_primitive(137, "caml_is_printable", unwrap_spec=[char])
def func(interpreter, char):
    raise NotImplementedError("missing")

@expose_primitive(138, "caml_lazy_follow_forward", unwrap_spec=[value])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(139, "caml_lazy_make_forward", unwrap_spec=[value])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(140, "caml_ldexp_float", unwrap_spec=[float, int])
def func(interpreter, f, i):
    raise NotImplementedError("missing")

@expose_primitive(141, "caml_le_float", unwrap_spec=[float, float])
def func(interpreter, f, g):
    raise NotImplementedError("missing")

@expose_primitive(142, "caml_lessequal", unwrap_spec=[value, value])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(143, "caml_lessthan", unwrap_spec=[value, value])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

# @expose_primitive(144, "caml_lex_engine", unwrap_spec=[struct lexing_table *tbl, start_state, struct lexer_buffer *lexbuf])
# def func(interpreter, struct lexing_table *tbl, start_state, struct lexer_buffer *lexbuf):
#     raise NotImplementedError("missing")

@expose_primitive(144, "caml_lex_engine", unwrap_spec=[unit, unit, unit])
def func(interpreter, _unit1, _unit2, _unit3):
    raise UnsupportedPrimitiveError

@expose_primitive(145, "caml_log10_float", unwrap_spec=[float])
def func(interpreter, f):
    raise NotImplementedError("missing")

@expose_primitive(146, "caml_log1p_float", unwrap_spec=[float])
def func(interpreter, f):
    raise NotImplementedError("missing")

@expose_primitive(147, "caml_log_float", unwrap_spec=[float])
def func(interpreter, f):
    raise NotImplementedError("missing")

@expose_primitive(148, "caml_lt_float", unwrap_spec=[float, float])
def func(interpreter, f, g):
    raise NotImplementedError("missing")

@expose_primitive(149, "caml_make_array", unwrap_spec=[value])
def func(interpreter, init):
    raise NotImplementedError("missing")

@expose_primitive(150, "caml_make_vect", unwrap_spec=[int, value])
def func(interpreter, length, w_init):
    if length == 0:
        return interpreter.space.atom(0)
    if isinstance(w_init, model.W_Float):
        raise NotImplementedError("float/double array")
    return interpreter.space.wrap_array([w_init] * length, "array")

@expose_primitive(151, "caml_marshal_data_size", unwrap_spec=[value, int])
def func(interpreter, buff, ofs):
    raise NotImplementedError("missing")

@expose_primitive(152, "caml_md5_chan", unwrap_spec=[channel, int])
def func(interpreter, vchan, length):
    raise NotImplementedError("missing")

@expose_primitive(153, "caml_md5_string", unwrap_spec=[str, int, int])
def func(interpreter, str, ofs, length):
    raise NotImplementedError("missing")

@expose_primitive(154, "caml_ml_channel_size", unwrap_spec=[channel])
def func(interpreter, vchannel):
    raise NotImplementedError("missing")

@expose_primitive(155, "caml_ml_channel_size_64", unwrap_spec=[channel])
def func(interpreter, vchannel):
    raise NotImplementedError("missing")

@expose_primitive(156, "caml_ml_close_channel", unwrap_spec=[channel])
def func(interpreter, vchannel):
    raise NotImplementedError("missing")

@expose_primitive(157, "caml_ml_flush", unwrap_spec=[channel])
def func(interpreter, vchannel):
    import os
    if vchannel != -1:
        os.fsync(vchannel)

@expose_primitive(158, "caml_ml_flush_partial", unwrap_spec=[channel])
def func(interpreter, vchannel):
    raise NotImplementedError("missing")

@expose_primitive(159, "caml_ml_input", unwrap_spec=[channel, value, int, int])
def func(interpreter, vchannel, buff, vstart, vlength):
    raise NotImplementedError("missing")

@expose_primitive(160, "caml_ml_input_char", unwrap_spec=[channel])
def func(interpreter, vchannel):
    raise NotImplementedError("missing")

@expose_primitive(161, "caml_ml_input_int", unwrap_spec=[channel])
def func(interpreter, vchannel):
    raise NotImplementedError("missing")

@expose_primitive(162, "caml_ml_input_scan_line", unwrap_spec=[channel])
def func(interpreter, vchannel):
    raise NotImplementedError("missing")

@expose_primitive(163, "caml_ml_open_descriptor_in", unwrap_spec=[int])
def func(interpreter, fd):
    return io.W_Channel.open_descriptor(fd)

@expose_primitive(164, "caml_ml_open_descriptor_out", unwrap_spec=[int])
def func(interpreter, fd):
    return io.W_Channel.open_descriptor(fd)

@expose_primitive(165, "caml_ml_out_channels_list", unwrap_spec=[unit])
def func(interpreter):
    return interpreter.space.wrap_list(io.W_Channel.open_channels())

@expose_primitive(166, "caml_ml_output", unwrap_spec=[channel, value, int, int])
def func(interpreter, vchannel, buff, start, length):
    raise NotImplementedError("missing")

@expose_primitive(167, "caml_ml_output_char", unwrap_spec=[channel, char])
def func(interpreter, vchannel, ch):
    raise NotImplementedError("missing")

@expose_primitive(168, "caml_ml_output_int", unwrap_spec=[channel, int])
def func(interpreter, vchannel, w):
    raise NotImplementedError("missing")

@expose_primitive(169, "caml_ml_output_partial", unwrap_spec=[channel, value, int, int])
def func(interpreter, vchannel, buff, start, length):
    raise NotImplementedError("missing")

@expose_primitive(170, "caml_ml_pos_in", unwrap_spec=[channel])
def func(interpreter, vchannel):
    raise NotImplementedError("missing")

@expose_primitive(171, "caml_ml_pos_in_64", unwrap_spec=[channel])
def func(interpreter, vchannel):
    raise NotImplementedError("missing")

@expose_primitive(172, "caml_ml_pos_out", unwrap_spec=[channel])
def func(interpreter, vchannel):
    raise NotImplementedError("missing")

@expose_primitive(173, "caml_ml_pos_out_64", unwrap_spec=[channel])
def func(interpreter, vchannel):
    raise NotImplementedError("missing")

@expose_primitive(174, "caml_ml_seek_in", unwrap_spec=[channel, int])
def func(interpreter, vchannel, pos):
    raise NotImplementedError("missing")

@expose_primitive(175, "caml_ml_seek_in_64", unwrap_spec=[channel, int])
def func(interpreter, vchannel, pos):
    raise NotImplementedError("missing")

@expose_primitive(176, "caml_ml_seek_out", unwrap_spec=[channel, int])
def func(interpreter, vchannel, pos):
    raise NotImplementedError("missing")

@expose_primitive(177, "caml_ml_seek_out_64", unwrap_spec=[channel, int])
def func(interpreter, vchannel, pos):
    raise NotImplementedError("missing")

@expose_primitive(178, "caml_ml_set_binary_mode", unwrap_spec=[channel, int])
def func(interpreter, vchannel, mode):
    raise NotImplementedError("missing")

@expose_primitive(179, "caml_ml_string_length", unwrap_spec=[str])
def func(interpreter, s):
    raise NotImplementedError("missing")

@expose_primitive(180, "caml_modf_float", unwrap_spec=[float])
def func(interpreter, f):
    raise NotImplementedError("missing")

@expose_primitive(181, "caml_mul_float", unwrap_spec=[float, float])
def func(interpreter, f, g):
    raise NotImplementedError("missing")

@expose_primitive(182, "caml_nativeint_add", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(183, "caml_nativeint_and", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(184, "caml_nativeint_bswap", unwrap_spec=[int])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(185, "caml_nativeint_compare", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(186, "caml_nativeint_div", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(187, "caml_nativeint_format", unwrap_spec=[str, int])
def func(interpreter, fmt, arg):
    raise NotImplementedError("missing")

@expose_primitive(188, "caml_nativeint_mod", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(189, "caml_nativeint_mul", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(190, "caml_nativeint_neg", unwrap_spec=[int])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(191, "caml_nativeint_of_float", unwrap_spec=[float])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(192, "caml_nativeint_of_int", unwrap_spec=[int])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(193, "caml_nativeint_of_int32", unwrap_spec=[int])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(194, "caml_nativeint_of_string", unwrap_spec=[str])
def func(interpreter, s):
    raise NotImplementedError("missing")

@expose_primitive(195, "caml_nativeint_or", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(196, "caml_nativeint_shift_left", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(197, "caml_nativeint_shift_right", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(198, "caml_nativeint_shift_right_unsigned", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(199, "caml_nativeint_sub", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(200, "caml_nativeint_to_float", unwrap_spec=[int])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(201, "caml_nativeint_to_int", unwrap_spec=[int])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(202, "caml_nativeint_to_int32", unwrap_spec=[int])
def func(interpreter, v):
    raise NotImplementedError("missing")

@expose_primitive(203, "caml_nativeint_xor", unwrap_spec=[int, int])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(204, "caml_neg_float", unwrap_spec=[float])
def func(interpreter, f):
    raise NotImplementedError("missing")

@expose_primitive(205, "caml_neq_float", unwrap_spec=[float, float])
def func(interpreter, f, g):
    raise NotImplementedError("missing")

# @expose_primitive(206, "caml_new_lex_engine", unwrap_spec=[struct lexing_table *tbl, start_state, struct lexer_buffer *lexbuf])
# def func(interpreter, struct lexing_table *tbl, start_state, struct lexer_buffer *lexbuf):
#     raise NotImplementedError("missing")
@expose_primitive(206, "caml_new_lex_engine", unwrap_spec=[unit, unit, unit])
def func(interpreter, _unit1, _unit2, _unit3):
    raise UnsupportedPrimitiveError

@expose_primitive(207, "caml_notequal", unwrap_spec=[value, value])
def func(interpreter, v1, v2):
    raise NotImplementedError("missing")

@expose_primitive(208, "caml_obj_add_offset", unwrap_spec=[value, int])
def func(interpreter, v, offset):
    raise NotImplementedError("missing")

@expose_primitive(209, "caml_obj_block", unwrap_spec=[value, int])
def func(interpreter, tag, size):
    raise NotImplementedError("missing")

@expose_primitive(210, "caml_obj_dup", unwrap_spec=[value])
def func(interpreter, arg):
    raise NotImplementedError("missing")

@expose_primitive(211, "caml_obj_is_block", unwrap_spec=[value])
def func(interpreter, arg):
    raise NotImplementedError("missing")

@expose_primitive(212, "caml_obj_set_tag", unwrap_spec=[value, value])
def func(interpreter, arg, new_tag):
    raise NotImplementedError("missing")

@expose_primitive(213, "caml_obj_tag", unwrap_spec=[value])
def func(interpreter, arg):
    raise NotImplementedError("missing")

@expose_primitive(214, "caml_obj_truncate", unwrap_spec=[value, int])
def func(interpreter, v, newsize):
    raise NotImplementedError("missing")

@expose_primitive(215, "caml_output_value", unwrap_spec=[channel, value, list])
def func(interpreter, vchan, v, flags):
    raise NotImplementedError("missing")

@expose_primitive(216, "caml_output_value_to_buffer", unwrap_spec=[value, int, int, value, list])
def func(interpreter, buf, ofs, length, v, flags):
    raise NotImplementedError("missing")

@expose_primitive(217, "caml_output_value_to_string", unwrap_spec=[value, list])
def func(interpreter, v, flags):
    raise NotImplementedError("missing")

# @expose_primitive(218, "caml_parse_engine", unwrap_spec=[struct parser_tables *tables, struct parser_env *env, cmd, arg])
# def func(interpreter, struct parser_tables *tables, struct parser_env *env, cmd, arg):
#     raise NotImplementedError("missing")
@expose_primitive(218, "caml_parse_engine", unwrap_spec=[unit, unit, unit, unit])
def func(interpreter, _unit1, _unit2, _unit3, _unit4):
    raise UnsupportedPrimitiveError

@expose_primitive(219, "caml_power_float", unwrap_spec=[float, float])
def func(interpreter, f, g):
    raise NotImplementedError("missing")

@expose_primitive(220, "caml_realloc_global", unwrap_spec=[int])
def func(interpreter, size):
    raise NotImplementedError("missing")

@expose_primitive(221, "caml_record_backtrace", unwrap_spec=[int])
def func(interpreter, vflag):
    raise NotImplementedError("missing")

# @expose_primitive(222, "caml_register_code_fragment", unwrap_spec=[prog, int, str])
# def func(interpreter, prog, length, digest):
#     raise NotImplementedError("missing")
@expose_primitive(222, "caml_register_code_fragment", unwrap_spec=[unit, unit, unit])
def func(interpreter, _unit1, _unit2, _unit3):
    raise UnsupportedPrimitiveError

@expose_primitive(223, "caml_register_named_value", unwrap_spec=[str, value])
def func(interpreter, vname, val):
    interpreter.register_named_value(vname, val)

# @expose_primitive(224, "caml_reify_bytecode", unwrap_spec=[prog, int])
# def func(interpreter, prog, length):
#     raise NotImplementedError("missing")
@expose_primitive(224, "caml_reify_bytecode", unwrap_spec=[unit, unit])
def func(interpreter, _unit1, _unit2):
    raise UnsupportedPrimitiveError

@expose_primitive(225, "caml_set_parser_trace", unwrap_spec=[int])
def func(interpreter, flag):
    raise NotImplementedError("missing")

@expose_primitive(226, "caml_sin_float", unwrap_spec=[float])
def func(interpreter, f):
    raise NotImplementedError("missing")

@expose_primitive(227, "caml_sinh_float", unwrap_spec=[float])
def func(interpreter, f):
    raise NotImplementedError("missing")

@expose_primitive(228, "caml_sqrt_float", unwrap_spec=[float])
def func(interpreter, f):
    raise NotImplementedError("missing")

@expose_primitive(229, "caml_static_alloc", unwrap_spec=[int])
def func(interpreter, size):
    raise NotImplementedError("missing")

@expose_primitive(230, "caml_static_free", unwrap_spec=[value])
def func(interpreter, blk):
    raise NotImplementedError("missing")

@expose_primitive(231, "caml_static_release_bytecode", unwrap_spec=[value, int])
def func(interpreter, blk, size):
    raise NotImplementedError("missing")

@expose_primitive(232, "caml_static_resize", unwrap_spec=[value, int])
def func(interpreter, blk, new_size):
    raise NotImplementedError("missing")

@expose_primitive(233, "caml_string_compare", unwrap_spec=[str, str])
def func(interpreter, s1, s2):
    raise NotImplementedError("missing")

@expose_primitive(234, "caml_string_equal", unwrap_spec=[str, str])
def func(interpreter, s1, s2):
    raise NotImplementedError("missing")

@expose_primitive(235, "caml_string_get", unwrap_spec=[str, int])
def func(interpreter, str, index):
    raise NotImplementedError("missing")

@expose_primitive(236, "caml_string_get16", unwrap_spec=[str, int])
def func(interpreter, str, index):
    raise NotImplementedError("missing")

@expose_primitive(237, "caml_string_get32", unwrap_spec=[str, int])
def func(interpreter, str, index):
    raise NotImplementedError("missing")

@expose_primitive(238, "caml_string_get64", unwrap_spec=[str, int])
def func(interpreter, str, index):
    raise NotImplementedError("missing")

@expose_primitive(239, "caml_string_greaterequal", unwrap_spec=[str, str])
def func(interpreter, s1, s2):
    raise NotImplementedError("missing")

@expose_primitive(240, "caml_string_greaterthan", unwrap_spec=[str, str])
def func(interpreter, s1, s2):
    raise NotImplementedError("missing")

@expose_primitive(241, "caml_string_lessequal", unwrap_spec=[str, str])
def func(interpreter, s1, s2):
    raise NotImplementedError("missing")

@expose_primitive(242, "caml_string_lessthan", unwrap_spec=[str, str])
def func(interpreter, s1, s2):
    raise NotImplementedError("missing")

@expose_primitive(243, "caml_string_notequal", unwrap_spec=[str, str])
def func(interpreter, s1, s2):
    raise NotImplementedError("missing")

@expose_primitive(244, "caml_string_set", unwrap_spec=[str, int, value])
def func(interpreter, str, index, newval):
    raise NotImplementedError("missing")

@expose_primitive(245, "caml_string_set16", unwrap_spec=[str, int, value])
def func(interpreter, str, index, newval):
    raise NotImplementedError("missing")

@expose_primitive(246, "caml_string_set32", unwrap_spec=[str, int, value])
def func(interpreter, str, index, newval):
    raise NotImplementedError("missing")

@expose_primitive(247, "caml_string_set64", unwrap_spec=[str, int, value])
def func(interpreter, str, index, newval):
    raise NotImplementedError("missing")

@expose_primitive(248, "caml_sub_float", unwrap_spec=[float, float])
def func(interpreter, f, g):
    raise NotImplementedError("missing")

@expose_primitive(249, "caml_sys_chdir", unwrap_spec=[str])
def func(interpreter, dirname):
    raise NotImplementedError("missing")

@expose_primitive(250, "caml_sys_close", unwrap_spec=[int])
def func(interpreter, fd):
    raise NotImplementedError("missing")

@jit.elidable
@expose_primitive(251, "caml_sys_const_big_endian", unwrap_spec=[unit])
def func(interpreter):
    return interpreter.space.false()

@jit.elidable
@expose_primitive(252, "caml_sys_const_ostype_cygwin", unwrap_spec=[unit])
def func(interpreter):
    #hack
    return interpreter.space.false()

@jit.elidable
@expose_primitive(253, "caml_sys_const_ostype_unix", unwrap_spec=[unit])
def func(interpreter):
    #hack
    return interpreter.space.true()

@jit.elidable
@expose_primitive(254, "caml_sys_const_ostype_win32", unwrap_spec=[unit])
def func(interpreter):
    #hack
    return interpreter.space.false()

@jit.elidable
@expose_primitive(255, "caml_sys_const_word_size", unwrap_spec=[unit])
def func(interpreter):
    from rpython.jit.backend.llsupport import symbolic
    return interpreter.space.wrap_int(symbolic.WORD * 8)

@expose_primitive(256, "caml_sys_exit", unwrap_spec=[int])
def func(interpreter, retcode):
    raise NotImplementedError("missing")

@expose_primitive(257, "caml_sys_file_exists", unwrap_spec=[str])
def func(interpreter, name):
    raise NotImplementedError("missing")

@jit.elidable
@expose_primitive(258, "caml_sys_get_argv", unwrap_spec=[unit])
def func(interpreter):
    exe_name = interpreter.space.wrap_string(
        interpreter.space.exe_name)
    args = interpreter.space.make_block(
        [interpreter.space.wrap_string(s) for s in interpreter.space.args])
    return interpreter.space.make_block([exe_name, args])

@jit.elidable
@expose_primitive(259, "caml_sys_get_config", unwrap_spec=[unit])
def func(interpreter):
    from rpython.jit.backend.llsupport import symbolic
    return interpreter.space.make_block([
        # ok, we are unix just now
        interpreter.space.wrap_string("Unix"),
        interpreter.space.wrap_int(symbolic.WORD * 8),
        interpreter.space.false(),
    ])

@expose_primitive(260, "caml_sys_getcwd", unwrap_spec=[unit])
def func(interpreter):
    raise NotImplementedError("missing")

@expose_primitive(261, "caml_sys_getenv", unwrap_spec=[str])
def func(interpreter, var):
    raise NotImplementedError("missing")

@expose_primitive(262, "caml_sys_is_directory", unwrap_spec=[str])
def func(interpreter, name):
    raise NotImplementedError("missing")

@expose_primitive(263, "caml_sys_open", unwrap_spec=[str, list, int])
def func(interpreter, path, vflags, vperm):
    raise NotImplementedError("missing")

@expose_primitive(264, "caml_sys_random_seed", unwrap_spec=[unit])
def func(interpreter):
    raise NotImplementedError("missing")

@expose_primitive(265, "caml_sys_read_directory", unwrap_spec=[str])
def func(interpreter, path):
    raise NotImplementedError("missing")

@expose_primitive(266, "caml_sys_remove", unwrap_spec=[str])
def func(interpreter, name):
    raise NotImplementedError("missing")

@expose_primitive(267, "caml_sys_rename", unwrap_spec=[str, str])
def func(interpreter, oldname, newname):
    raise NotImplementedError("missing")

@expose_primitive(268, "caml_sys_system_command", unwrap_spec=[str])
def func(interpreter, command):
    raise NotImplementedError("missing")

@expose_primitive(269, "caml_sys_time", unwrap_spec=[unit])
def func(interpreter):
    raise NotImplementedError("missing")

@expose_primitive(270, "caml_tan_float", unwrap_spec=[float])
def func(interpreter, f):
    raise NotImplementedError("missing")

@expose_primitive(271, "caml_tanh_float", unwrap_spec=[float])
def func(interpreter, f):
    raise NotImplementedError("missing")

@expose_primitive(272, "caml_terminfo_backup", unwrap_spec=[int])
def func(interpreter, lines):
    raise NotImplementedError("missing")

@expose_primitive(273, "caml_terminfo_resume", unwrap_spec=[int])
def func(interpreter, lines):
    raise NotImplementedError("missing")

@expose_primitive(274, "caml_terminfo_setup", unwrap_spec=[channel])
def func(interpreter, vchan):
    raise NotImplementedError("missing")

@expose_primitive(275, "caml_terminfo_standout", unwrap_spec=[int])
def func(interpreter, start):
    raise NotImplementedError("missing")

@expose_primitive(276, "caml_update_dummy", unwrap_spec=[value, value])
def func(interpreter, dummy, newval):
    raise NotImplementedError("missing")

@expose_primitive(277, "caml_weak_blit", unwrap_spec=[list, int, list, int, int])
def func(interpreter, ars, ofs, ard, ofd, length):
    raise NotImplementedError("missing")

@expose_primitive(278, "caml_weak_check", unwrap_spec=[list, int])
def func(interpreter, ar, n):
    raise NotImplementedError("missing")

@expose_primitive(279, "caml_weak_create", unwrap_spec=[int])
def func(interpreter, length):
    raise NotImplementedError("missing")

@expose_primitive(280, "caml_weak_get", unwrap_spec=[list, int])
def func(interpreter, ar, n):
    raise NotImplementedError("missing")

@expose_primitive(281, "caml_weak_get_copy", unwrap_spec=[list, int])
def func(interpreter, ar, n):
    raise NotImplementedError("missing")

@expose_primitive(282, "caml_weak_set", unwrap_spec=[list, int, value])
def func(interpreter, ar, n, el):
    raise NotImplementedError("missing")
# EOF
