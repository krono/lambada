#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
from rpython.rlib.streamio import open_file_as_stream
from rpython.rlib.objectmodel import specialize
from rpython.rlib.rstruct.runpack import runpack
from rpython.rlib.unroll import unrolling_iterable
from rpython.rlib.objectmodel import we_are_translated

from .tag import *
from ..error import CorruptBytecodeError, UnsupportedBytecodeError

TRAILER_SIZE = (4 + 12)

EXEC_MAGIC = "Caml1999X008"
INTEXT_MAGIC_NUMBER = 0x8495A6BE

CODE_INT8                  = 0x00
CODE_INT16                 = 0x01
CODE_INT32                 = 0x02
CODE_INT64                 = 0x03
CODE_SHARED8               = 0x04
CODE_SHARED16              = 0x05
CODE_SHARED32              = 0x06
#
CODE_DOUBLE_ARRAY32_LITTLE = 0x07
#
CODE_BLOCK32               = 0x08
#
CODE_STRING8               = 0x09
CODE_STRING32              = 0x0A
CODE_DOUBLE_BIG            = 0x0B
CODE_DOUBLE_LITTLE         = 0x0C
CODE_DOUBLE_ARRAY8_BIG     = 0x0D
CODE_DOUBLE_ARRAY8_LITTLE  = 0x0E
CODE_DOUBLE_ARRAY32_BIG    = 0x0F
CODE_CODEPOINTER           = 0x10
CODE_INFIXPOINTER          = 0x11
CODE_CUSTOM                = 0x12
CODE_BLOCK64               = 0x13
#
PREFIX_SMALL_STRING        = 0x20
PREFIX_SMALL_INT           = 0x40
PREFIX_SMALL_BLOCK         = 0x80

_indent = [0]
def log(strn, num):
    print "  " * _indent[0],
    print "%s, %d" %(strn, num)
def indent():
    _indent[0] += 1
def outdent():
    global _indent
    _indent[0] -= 1


def caml_tag(integer):
    return integer & 0xFF
def caml_wo_size(integer):
    return integer >> 10


def interpret64(b, unsigned=False):
    assert len(b) == 8
    if unsigned:
        return runpack("<Q", b)
    else:
        return runpack("<q", b)

def interpret32(b, unsigned=False):
    assert len(b) == 4
    if unsigned:
        return runpack("<L", b)
    else:
        return runpack("<l", b)



# ____________________________________________________________
#
# Reads an bytecode file and creates all model objects

class Stream(object):
    """ Simple input stream """
    def __init__(self, inputfile=None, data=None):
        if inputfile is None and data is None:
            raise RuntimeError("need to supply either inputfile or data")

        if inputfile:
            try:
                self.data = inputfile.readall()
            finally:
                inputfile.close()
        else:
            self.data = data
        self.reset()

    def peek(self, unsigned=False):
        if self.pos >= len(self.data):
            raise IndexError
        start, stop = self.pos, self.pos + self.word_size
        assert start >= 0 and stop >= 0
        data_peek = self.data[start:stop]
        if self.use_long_read:
            return interpret64(data_peek, unsigned)
        else:
            return interpret32(data_peek, unsigned)

    def next(self, unsigned=False):
        return (self.next_words(1, unsigned))[0]

    def next_words(self, count, unsigned=False):
        words = [0] * count
        for index in range(count):
            words[index] = self.peek(unsigned)
            self.pos += self.word_size
        return words

    def next_bytes(self, count):
        start, stop = self.pos, self.pos + count
        assert start >= 0 and stop >= 0
        try:
            return self.data[start:stop]
        finally:
            self.pos += count

    def next_byte(self):
        return self.next_bytes(1)

    @specialize.arg(1)
    def _unpack(self, fmt, val, big_endian=False):
        if big_endian:
            return self._unpack_be(fmt, val)
        else:
            return self._unpack_le(fmt, val)

    @specialize.arg(1)
    def _unpack_be(self, fmt, val):
        return runpack(">" + fmt, val)

    @specialize.arg(1)
    def _unpack_le(self, fmt, val):
        return runpack("<" + fmt, val)

    def next_int8(self, unsigned=False, big_endian=True):
        val = self.next_bytes(1)
        if unsigned:
            return self._unpack("B", val, big_endian)
        else:
            return self._unpack("b", val, big_endian)

    def next_int16(self, unsigned=False, big_endian=True):
        val = self.next_bytes(2)
        if unsigned:
            return self._unpack("H", val, big_endian)
        else:
            return self._unpack("h", val, big_endian)

    def next_int32(self, unsigned=False, big_endian=True):
        val = self.next_bytes(4)
        if unsigned:
            return self._unpack("I", val, big_endian)
        else:
            return self._unpack("i", val, big_endian)

    def next_int64(self, unsigned=False, big_endian=True):
        val = self.next_bytes(8)
        if unsigned:
            return self._unpack("Q", val, big_endian)
        else:
            return self._unpack("q", val, big_endian)

    def next_double(self, big_endian=True):
        val = self.next_bytes(8)
        return self._unpack("d", val, big_endian)

    def next_uint32be(self):
        return self.next_int32(unsigned=True, big_endian=True)

    def reset(self):
        self.swap = False
        self.pos = 0
        self.be_32bit()

    def skipbytes(self, jump):
        assert jump > 0
        assert (self.pos + jump) <= len(self.data)
        self.pos += jump

    def skipwords(self, jump):
        self.skipbytes(jump * self.word_size)
        assert (self.pos + jump) <= len(self.data)
        self.pos += jump

    def seek(self, num, whence):
        if whence == 0:
            self.pos = num
        elif whence == 1:
            self.pos += num
        elif whence == 2:
            newpos = num + self.length()
            assert newpos < self.length()
            self.pos = newpos
        else:
            raise IndexError

    def length(self):
        return len(self.data)

    def close(self):
        pass # already closed

    def be_64bit(self):
        self.word_size = 8
        self.use_long_read = True

    def be_32bit(self):
        self.word_size = 4
        self.use_long_read = False

    def tell(self):
        return self.pos



class ImageReader(object):

    def __init__(self, space, stream):
        self.space = space
        self.stream = stream
        self.sections = {}
        self._section_infos = {}
        self.intcache = {}
        self.code = []
        self.global_data = []
        #

    def _getword(self):
        return self.stream.next_uint32be()

    def read_all(self):
        self.read_trailer()
        for sec in self._section_infos:
            if not sec in ["CODE", "DATA", "PRIM"]:
                self.sections[sec] = self.read_section_raw(sec)
        self.code = self.read_code()
        self.global_data = self.read_data()

    def read_trailer(self):
        """
        Read the bytecode file information. AT END OF FILE,
        hence trailer, not header
        """
        self.stream.seek(-TRAILER_SIZE, 2)
        num_sections = self.stream.next_uint32be()
        magic = self.stream.next_bytes(12)
        assert magic == EXEC_MAGIC

        self.read_sections(num_sections)

    def read_sections(self, num_sections):
        toc_size = num_sections * 8
        self.stream.seek(- (TRAILER_SIZE + toc_size), 2)
        sects = [self.read_section_descriptor() for _ in range(num_sections)]
        lookbehind = TRAILER_SIZE + toc_size
        for _, length in sects:
            lookbehind += length
        self.stream.seek(-lookbehind, 2)
        pos = self.stream.tell()
        self.stream.seek(pos, 0)
        pos2 = self.stream.tell()
        assert pos == pos2
        for key, length in sects:
            self._section_infos[key] = (pos, length)
            pos += length
        for sec in ["CODE", "DATA", "PRIM"]:
            assert sec in self._section_infos

    def read_section_raw(self, name):
        pos, length =  self._section_infos[name]
        old_pos = self.stream.tell()
        try:
            self.stream.seek(pos, 0)
            return self.stream.next_bytes(length)
        finally:
            self.stream.seek(old_pos, 0)

    def read_section_descriptor(self):
        name = self.stream.next_bytes(4)
        length = self.stream.next_uint32be()
        return (name, length)

    def read_code(self):
        pos, length = self._section_infos["CODE"]
        old_pos = self.stream.tell()
        try:
            self.stream.seek(pos, 0)
            return self._read_code(length)
        finally:
            self.stream.seek(old_pos, 0)

    def _read_code(self, length):
        return self.stream.next_words(length / 4)

    def read_data(self):
        pos, length = self._section_infos["DATA"]
        old_pos = self.stream.tell()
        try:
            self.stream.seek(pos, 0)
            return self._read_data()
        finally:
            self.stream.seek(old_pos, 0)

    def _read_data(self):
        magic = self._getword()
        assert magic == INTEXT_MAGIC_NUMBER

        block_len = self._getword()
        num_objects = self._getword()
        size32 = self._getword()
        size64 = self._getword()

        whsize = size64
        wosize = whsize - 1

        # print num_objects, block_len, size64, size32
        print num_objects, block_len, whsize, wosize
        self._objs = [None] * (num_objects)
        self._obj_count = 0
        w_block = self.__read_next()
        assert self.space.is_block(w_block)
        return self.space.get_block_content(w_block)

    def __read_next(self):
        code = self.stream.next_int8(unsigned=True)
        log("code: (%s)" % hex(code), code)
        indent()
        if False: pass
        elif code >= PREFIX_SMALL_BLOCK:
            log("small block", 0)
            tag = code & 0xF
            size = (code >> 4) & 0x7
            v = self.__read_block(tag, size)
        elif code >= PREFIX_SMALL_INT:
            v = self.space.wrap_int(code & 0x3F)
            log( "small int: ", self.space.unwrap_int(v))
        elif code >= PREFIX_SMALL_STRING:
            log( "small string", 0)
            strlen = code & 0x1f
            v = self.__read_string(strlen)
        elif code == CODE_INT8:
            v = self.space.wrap_int(self.stream.next_int8())
            log( "int8: ", self.space.unwrap_int(v))
        elif code == CODE_INT16:
            v = self.space.wrap_int(self.stream.next_int16())
            log( "int16: ", self.space.unwrap_int(v))
        elif code == CODE_INT32:
            v = self.space.wrap_int(self.stream.next_int32())
            log( "int32: ", self.space.unwrap_int(v))
        elif code == CODE_INT64:
            v = self.space.wrap_int(self.stream.next_int64())
            log( "int64: ", self.space.unwrap_int(v))
        elif code == CODE_SHARED8:
            log( "shared8", 0)
            offset = self.stream.next_int8(unsigned=True)
            v = self._objs[self._obj_count - offset]
        elif code == CODE_SHARED16:
            log( "shared16", 0)
            offset = self.stream.next_int16(unsigned=True)
            v = self._objs[self._obj_count - offset]
        elif code == CODE_SHARED32:
            log( "shared32", 0)
            offset = self.stream.next_int32(unsigned=True)
            v = self._objs[self._obj_count - offset]
        elif code == CODE_BLOCK32:
            hdr = self.stream.next_int32(unsigned=True)
            _tag, _size = caml_tag(hdr), caml_wo_size(hdr)
            log( "block32(%d,%d)" % (_tag, _size), hdr)
            v = self.__read_block(_tag, _size)
        elif code == CODE_BLOCK64:
            log( "block64", 0)
            hdr = self.stream.next_int64()
            v = self.__read_block(caml_tag(hdr), caml_wo_size(hdr))
        elif code == CODE_STRING8:
            log( "string8", 0)
            length = self.stream.next_int8(unsigned=True)
            v = self.__read_string(length)
        elif code == CODE_STRING32:
            log( "string32", 0)
            length = self.stream.next_int32(unsigned=True)
            v = self.__read_string(length)
        elif code in [CODE_DOUBLE_LITTLE, CODE_DOUBLE_BIG]:
            v = self.space.wrap_float(
                self.stream.next_double(big_endian=(code==CODE_DOUBLE_BIG)))
            log( "double %f" % self.space.unwrap_float(v), 0)
        elif code == CODE_CUSTOM:
            log( "custom", 0)
            v = self.__read_custom()
        else:
            raise NotImplementedError
        outdent()
        return v

    def __object_index(self):
        try:
            print self._obj_count
            return self._obj_count
        finally:
            self._obj_count += 1

    def __register_object(self, val, idx=-1):
        if idx < 0:
            index = 0
        else:
            index = self.__object_index()
        assert index >= 0
        self._objs[index] = val

    def __read_block(self, tag, size):
        log( "blk(%s): " % tag, size)
        if size == 0:
            return self.space.atom(tag)
        else:
            if tag == OBJECT_TAG:
                raise NotImplementedError
            assert tag == 0 #...
            idx = self.__object_index()
            itms = [self.__read_next() for i in range(size)]
            v = self.space.make_block(itms, tag)
            self.__register_object(v, idx)
            return v

    def __read_string(self, length):
        strs = self.stream.next_bytes(length)
        # Stuff here
        log("got string >%s< len:" % strs, length)
        w_str = self.space.make_string(strs)
        self.__register_object(w_str)
        return w_str

    def __read_custom(self):
        ops = ""
        c = self.stream.next_byte()
        while ord(c[0]) != 0:
            ops += c
            c = self.stream.next_byte()
        log( ops, 0)
        # HACK this is that we get stuff read for now
        if False: pass
        elif ops == "_j": #int64
            v = self.space.wrap_int(self.stream.next_int64())
        elif ops == "_i": #int32
            v = self.space.wrap_int(self.stream.next_int32())
        else:
            raise NotImplementedError
        log("custom>", self.space.unwrap_int(v))
        self.__register_object(v)
        return v

def read_bytecodefile(space, filename):
    if not os.path.exists(filename):
        os.write(2, "Error -- file not found: %s\n" % filename)
        return None
    try:
        stream = Stream(inputfile=open_file_as_stream(filename, mode="rb",
                                                      buffering=0))
        return ImageReader(space, stream)
    except OSError as e:
        os.write(2, "Error%s -- %s\n" % (os.strerror(e.errno), filename))
        return None
