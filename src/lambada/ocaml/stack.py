#!/usr/bin/env python
# -*- coding: utf-8 -*-


from ..stack import StackElement
from ..model import W_Value

class __extend__(StackElement):
    def pop(self):
        return (self._data, self._next)

class OCamlStackElement(StackElement):
    def push(self, hd):
        return OCamlStackElement(hd, self)

def stack(hd, tl=None):
    return OCamlStackElement(hd, tl)

class W_WrappedStack(W_Value):
    _immutable_fields_ = ["_sp"]
    def __init__(self, sp):
        self._sp = sp
    def sp(self):
        return self._sp

#EOF
