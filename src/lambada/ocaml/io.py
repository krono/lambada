#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os

from ..model import W_Value

os.stat_float_times(False)


try:
    std_fds = [sys.stdin.fileno(),
               sys.stdout.fileno(),
               sys.stderr.fileno()]
except ValueError:
    std_fds = [0, 1, 2]

class W_Channel(W_Value):

    _open = []

    def __init__(self, fd):
        self._fd = fd

    def unwrapped_fd(self):
        return self._fd

    @staticmethod
    def open_descriptor(fd):
        res = W_Channel(fd)
        W_Channel._open.append(res)

    @staticmethod
    def open_channels():
        return W_Channel._open
