#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Straight from instruct.h
"""

/* The instruction set. */

"""

from rpython.rlib import jit

from util.enum import Enumeration, EnumEntry

class Bytecode(EnumEntry):
    @jit.elidable
    def get_operand_count(self):
        return _get_bytecode_length(self.index())

class Instruction(Enumeration): (
    ACC0, ACC1, ACC2, ACC3, ACC4, ACC5, ACC6, ACC7,
    ACC, PUSH,
    PUSHACC0, PUSHACC1, PUSHACC2, PUSHACC3,
    PUSHACC4, PUSHACC5, PUSHACC6, PUSHACC7,
    PUSHACC, POP, ASSIGN,
    ENVACC1, ENVACC2, ENVACC3, ENVACC4, ENVACC,
    PUSHENVACC1, PUSHENVACC2, PUSHENVACC3, PUSHENVACC4, PUSHENVACC,
    PUSH_RETADDR, APPLY, APPLY1, APPLY2, APPLY3,
    APPTERM, APPTERM1, APPTERM2, APPTERM3,
    RETURN, RESTART, GRAB,
    CLOSURE, CLOSUREREC,
    OFFSETCLOSUREM2, OFFSETCLOSURE0, OFFSETCLOSURE2, OFFSETCLOSURE,
    PUSHOFFSETCLOSUREM2, PUSHOFFSETCLOSURE0,
    PUSHOFFSETCLOSURE2, PUSHOFFSETCLOSURE,
    GETGLOBAL, PUSHGETGLOBAL, GETGLOBALFIELD, PUSHGETGLOBALFIELD, SETGLOBAL,
    ATOM0, ATOM, PUSHATOM0, PUSHATOM,
    MAKEBLOCK, MAKEBLOCK1, MAKEBLOCK2, MAKEBLOCK3, MAKEFLOATBLOCK,
    GETFIELD0, GETFIELD1, GETFIELD2, GETFIELD3, GETFIELD, GETFLOATFIELD,
    SETFIELD0, SETFIELD1, SETFIELD2, SETFIELD3, SETFIELD, SETFLOATFIELD,
    VECTLENGTH, GETVECTITEM, SETVECTITEM,
    GETSTRINGCHAR, SETSTRINGCHAR,
    BRANCH, BRANCHIF, BRANCHIFNOT, SWITCH, BOOLNOT,
    PUSHTRAP, POPTRAP, RAISE, CHECK_SIGNALS,
    C_CALL1, C_CALL2, C_CALL3, C_CALL4, C_CALL5, C_CALLN,
    CONST0, CONST1, CONST2, CONST3, CONSTINT,
    PUSHCONST0, PUSHCONST1, PUSHCONST2, PUSHCONST3, PUSHCONSTINT,
    NEGINT, ADDINT, SUBINT, MULINT, DIVINT, MODINT,
    ANDINT, ORINT, XORINT, LSLINT, LSRINT, ASRINT,
    EQ, NEQ, LTINT, LEINT, GTINT, GEINT,
    OFFSETINT, OFFSETREF, ISINT,
    GETMETHOD,
    BEQ, BNEQ,  BLTINT, BLEINT, BGTINT, BGEINT,
    ULTINT, UGEINT,
    BULTINT, BUGEINT,
    GETPUBMET, GETDYNMET,
    STOP,
    EVENT, BREAK
) = range(146); \
    __enum_entry__ = Bytecode

class InvalidBytecode(Exception):
    def __init__(self, number):
        self.number = number

def _make_length():
    #
    # from fix_code.c
    #
    l = [0] * (Instruction.max() + 1)
    # /* Instructions with one operand */
    _ = l[Instruction.POP.index()] = l[Instruction.ASSIGN.index()] = \
        l[Instruction.PUSHACC.index()] = l[Instruction.ACC.index()] = \
        l[Instruction.PUSHENVACC.index()] = l[Instruction.ENVACC.index()] = \
        l[Instruction.PUSH_RETADDR.index()] = l[Instruction.APPLY.index()] = \
        l[Instruction.APPTERM1.index()] = l[Instruction.APPTERM2.index()] = \
        l[Instruction.APPTERM3.index()] = l[Instruction.RETURN.index()] = \
        l[Instruction.GRAB.index()] = l[Instruction.PUSHGETGLOBAL.index()] = \
        l[Instruction.GETGLOBAL.index()] = l[Instruction.SETGLOBAL.index()] = \
        l[Instruction.PUSHATOM.index()] = l[Instruction.ATOM.index()] = \
        l[Instruction.MAKEBLOCK1.index()] = \
        l[Instruction.MAKEBLOCK2.index()] = \
        l[Instruction.MAKEBLOCK3.index()] = \
        l[Instruction.MAKEFLOATBLOCK.index()] = \
        l[Instruction.GETFIELD.index()] = \
        l[Instruction.GETFLOATFIELD.index()] = \
        l[Instruction.SETFIELD.index()] = \
        l[Instruction.SETFLOATFIELD.index()] = \
        l[Instruction.BRANCH.index()] = \
        l[Instruction.BRANCHIF.index()] = \
        l[Instruction.BRANCHIFNOT.index()] = \
        l[Instruction.PUSHTRAP.index()] = l[Instruction.C_CALL1.index()] = \
        l[Instruction.C_CALL2.index()] = l[Instruction.C_CALL3.index()] = \
        l[Instruction.C_CALL4.index()] = l[Instruction.C_CALL5.index()] = \
        l[Instruction.CONSTINT.index()] = \
        l[Instruction.PUSHCONSTINT.index()] = \
        l[Instruction.OFFSETINT.index()] = l[Instruction.OFFSETREF.index()] = \
        l[Instruction.OFFSETCLOSURE.index()] = \
        l[Instruction.PUSHOFFSETCLOSURE.index()] = 1;

    # /* Instructions with two operands */
    l[Instruction.APPTERM.index()] = l[Instruction.CLOSURE.index()] = \
    l[Instruction.PUSHGETGLOBALFIELD.index()] = \
    l[Instruction.GETGLOBALFIELD.index()] = \
    l[Instruction.MAKEBLOCK.index()] = l[Instruction.C_CALLN.index()] = \
    l[Instruction.BEQ.index()] = l[Instruction.BNEQ.index()] = \
    l[Instruction.BLTINT.index()] = l[Instruction.BLEINT.index()] = \
    l[Instruction.BGTINT.index()] = l[Instruction.BGEINT.index()] = \
    l[Instruction.BULTINT.index()] = l[Instruction.BUGEINT.index()] = \
    l[Instruction.GETPUBMET.index()] = 2;

    #
    # variable-length:
    l[Instruction.SWITCH.index()] = l[Instruction.CLOSUREREC.index()] = -1

    return l


_bytecode_length = _make_length()

@jit.elidable
def _get_bytecode_length(index):
    assert index < len(_bytecode_length)
    return _bytecode_length[index]

@jit.elidable
def get_bytecode(bc):
    if bc < Instruction.max():
        return Instruction.at(bc)
    else:
        raise InvalidBytecode(bc)

if __name__ == '__main__':
    for i in range(Instruction.max()):
        bc = Instruction.at(i)
        print '{0:3d} {1:20s} [{2}]'.format(i, bc, bc.get_operand_count())
