#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import time

from rpython.rlib import jit
from rpython.rlib.debug import debug_start, debug_stop, debug_print

from lambada import space, interpreter
from lambada.ocaml import bytecodefile

from rpython.rlib.objectmodel import we_are_translated


space = space.Space()
# __________  Entry points  __________

def entry_point(argv):
    argv.pop(0)
    if len(argv) > 0:
        filename = argv.pop(0)
        print filename
        bcfile = bytecodefile.read_bytecodefile(space, filename)
        if bcfile:
            bcfile.read_all()
            for name, (pos, length) in bcfile._section_infos.items():
                print name, "@", hex(pos), "->", length
            space.initialize_globals(bcfile.global_data)
            space.initialize_args(filename, argv)
            ret = interpreter.run(space, bcfile.code)

            from lambada.model import W_Integer
            if isinstance(ret, W_Integer):
                print ret.value()
            else:
                print ret
            return 0
        else:
            return 1
    else:
        print "need bytecodefile"
    return 0

# _____ Define and setup target ___


def target(driver, args):
    driver.exe_name = 'lambada'
    return entry_point, None


def jitpolicy(driver):
    from rpython.jit.codewriter.policy import JitPolicy
    return JitPolicy()

if __name__ == '__main__':
    assert not we_are_translated()
    import util.debug
    from rpython.translator.driver import TranslationDriver
    f, _ = target(TranslationDriver(), sys.argv)
    try:
        sys.exit(f(sys.argv))
    except SystemExit:
        pass
    except:
        import pdb, traceback
        _type, value, tb = sys.exc_info()
        traceback.print_exception(_type, value, tb)
        pdb.post_mortem(tb)
