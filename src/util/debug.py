#!/usr/bin/env python
# -*- coding: utf-8 -*-

import py

from util.view import _dot, view
from util.repr import urepr, who, uni

_iteration = 0
_stacks = {}

from lambada.model import (Object, W_Value, W_Tag, W_Float, W_Integer,
                           W_Constructor, W_String)
from lambada.shape import Shape, CompoundShape, InStorageShape

from lambada.space import Space

from lambada.stack import StackElement

#
# Monkeypatch debug output
#

hard_debug = False

### General ###

class __extend__(Object):
    def __repr__(self):
        r = self.to_repr(set())
        r = r if isinstance(r, str) else r.encode("utf-8")
        if hard_debug:
            r += "<" + who(self).encode("utf-8") + ">"
        return r

    @uni
    def to_repr(self, seen):
        return object.__repr__(self)

class __extend__(StackElement):
    @uni
    def to_repr(self, seen):
        r = u""
        if self._data is not None:
            r += urepr(self._data, seen)
        if self._next is None:
            r += u"⊥"
        return r
    def linearize(self): # pragma: no cover
        element = self
        ret = [element._data] if element._data else []
        while element._next is not None:
            element = element._next
            ret.append(element._data)
        return ret

### Shapes ###

class __extend__(Shape):
    @uni
    def to_repr(self, seen):
        res = u"σ"
        res += u"%d" % self.get_number_of_direct_children()
        return res

class __extend__(CompoundShape):
    @uni
    def to_repr(self, seen):
        def mini_urepr(x):
            s = set(seen)
            s.discard(x)
            return urepr(x, s)

        res = u"σ"
        res += urepr(self._tag, seen)
        res += u"["
        res += ", ".join(map(mini_urepr, self._structure))
        res += u"]"
        return res

class __extend__(InStorageShape):
    @uni
    def to_repr(self, seen):
        return u"◊"

### Models ###


class __extend__(W_Tag):
    @uni
    def to_repr(self, seen):
        return u"%s/%d" % (self.name, self.arity())

class __extend__(W_Integer):
    @uni
    def to_repr(self, seen):
        return u"#%d" % self.value()

class __extend__(W_Float):
    @uni
    def to_repr(self, seen):
        return u"#%g" % self.value()

class __extend__(W_Constructor):
    def linearize(self):
        if self.get_tag().name == "cons" and self.get_tag().arity() ==2:
            l = []
            cons = self
            while not cons.get_tag().name == "unit":
                l.append(cons.get_child(0))
                cons = cons.get_child(1)
            return l
        else:
            raise NotImplementedError

    @uni
    def to_repr(self, seen):
        return u"Γ" + u"%s%s" % (urepr(self.get_tag(), seen), self.children_to_repr(seen))

    def children_to_repr(self, seen):
        def mini_urepr(x):
            s = set(seen)
            s.discard(x)
            return urepr(x, s)

        if self.get_number_of_children() > 0:
            return u"(" + u", ".join(map(mini_urepr, self.get_children())) + u")"
        else:
            return u""

#### Lambada!

class __extend__(W_String):
    @uni
    def to_repr(self, seen):
        return u"“" + unicode(self.value()) + u"”"

from lambada.ocaml.stack import OCamlStackElement
class __extend__(OCamlStackElement):
    @uni
    def to_repr(self, seen):
        l = []
        p = self
        while p is not None:
            l.append(p._data)
            p = p._next
        return u",".join(map(lambda x: urepr(x, seen), l))

class __extend__(W_Tag):
    @uni
    def to_repr(self, seen):
        if hasattr(self, 'value') and self.name == 'atom':
            return u"'[%d]" % (self.value)
        else:
            return u"%s/%d" % (self.name, self.arity())

class __extend__(Space):
    def print_globals(self):
        print self._global_data
###############################################################################

def debug_stack(stack):
    """
    print dictionary of stacks.
    """
    print
    #print "%: Cursor, !: Expression, μ: Call, #: Value, λ: Lambda, &: Pattern, {}: Rule, _ Variable"

    d = { 'ex_stack': stack.execution_stack, 'op_stack': stack.operand_stack }

    length = 60

    def i_(o):
        if hasattr(o, 'linearize'):
            return o.linearize()
        try: [ e for e in o ]
        except TypeError: return [o]
        else: return o
    def t_(o):
        return o[:length] if len(o) > length else o

    from itertools import izip_longest
    k = d.keys()
    v = map(list, map(list, map(i_, d.values())))

    update_stack_mappings(stack)

    stacklists = map(list, map(reversed, zip(*list(izip_longest(*v, fillvalue="")))))
    stackreprs = map(lambda x: map(lambda y: t_(urepr(y)) if y else u"", x), stacklists)
    stacks = map(lambda x: map(lambda y: (u"[%%-%ds]" % length) % y, x), stackreprs)

    tops = map(lambda x: [(u"%%-%ds" % (length + 3)) % x], k)
    stat = map(lambda x: x[0] + x[1], list(zip(tops,stacks)))
    lines = map(lambda x: u" ".join(x), map(list, zip(*stat)))
    print "\n".join(map(lambda x: x.encode("utf-8"), lines))

def update_stack_mappings(stack):
    """
    update the global stack mapping to be viewed with view_stacks()
    """
    import copy
    global _stacks
    global _iteration

    d = { 'ex_stack': stack.execution_stack, 'op_stack': stack.operand_stack }
    k = d.keys()
    for i in range(len(k)):
        localkey = "_%03d%s" % (_iteration, k[i])
        _stacks[localkey] = copy.copy(d[k[i]])
    _iteration += 1



def view_stacks():
    """
    view all stacks using dot/pygame as captured with update_stack_mappings()
    """
    view(**_stacks)


def storagewalker(l):
    def sw(l, seen):
        res = u"["
        first = True
        for item in l:
            if first:
                first = False
            else:
                res += u", "
            if not item in seen:
                seen.add(item)
                if hasattr(item, "get_storage"):
                    res += sw(item.get_storage(), seen)
                else:
                    res += urepr(item)
            else:
                res += u"…"
        res += u"]"
        return res
    return sw(l, set())
