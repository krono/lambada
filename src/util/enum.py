#!/usr/bin/env python
# -*- coding: utf-8 -*-

class EnumEntry(object):
    """
    An entry in an Enumeration
    """

    def __init__(self, enum_name, name, index):
        self._enum_name = enum_name
        self._name = name
        self._index = index

    def _set_enum(self, enum): self._enum = enum
    def index(self): return self._index
    def name(self): return self._name
    def __str__(self): return self.name()
    to_string = __str__
    def __repr__(self):
        if self._enum:
            return "<%s.%s: %d>" % (self._enum_name, self._name, self._index)
        else:
            return "<%s: %d>" % (self._name, self._index)
    to_repr = __repr__

class Enum(type):
    """
    Metaclass for Enums
    """
    def __new__(cls, clsname, bases, attributes):
        """NOT_RPYTHON"""
        for a in [attributes] + [base.__dict__ for base in bases]:
            if "__enum_entry__" in a:
                enum_entry = a["__enum_entry__"]
                break
        else:
            enum_entry = EnumEntry
        entries = {name: enum_entry(clsname, name, value) \
                   for name, value in attributes.items() \
                   if not name.startswith("_")}
        attributes.update(entries)
        reverse = [None for name in attributes if not name.startswith("_")]
        for name, entry in attributes.items():
            if isinstance(entry, enum_entry):
                reverse[entry.index()] = entry
        attributes["_entries"] = reverse
        attributes["_entries_dict"] = entries
        the_class = type.__new__(cls, clsname, bases, attributes)
        for entry in entries.values():
            entry._set_enum(the_class)

        def _at(index):
            assert index < len(the_class._entries)
            return the_class._entries[index]
        the_class.at = staticmethod(_at)
        def _named(name):
            return the_class._entries_dict[name]
        the_class.named = staticmethod(_named)
        def _items():
            return the_class._entries
        the_class.items = staticmethod(_items)
        def _max():
            return len(the_class._entries)
        the_class.max = staticmethod(_max)
        return the_class

class Enumeration(object):
    __metaclass__ = Enum

# EOF